/*libC++RWD Library for C++ Rendering of White_dune Data (in Development)*/

/* Copyright (c) Stefan Wolf, 2010. */

/*Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

#ifndef LIBCPPRWD_H_INCLUDED
#define LIBCPPRWD_H_INCLUDED

class X3dNode;

#include <GL/glu.h>
#include <GL/gl.h>
#ifdef WIN32
#include <windows.h>
#endif
#include <cstdio>
#include <math.h>

namespace CPPRWD
{

    void error(const char *errormsg);

    void IndexedFaceSetRender(X3dNode *data, void*);

    void PixelTextureRender(X3dNode *data, void*);

    void PointLightRender(X3dNode *data, void*);

    void MaterialRender(X3dNode *data, void*);

    void TransformTreeRender(X3dNode *data, void *dataptr);

    void GroupTreeRender(X3dNode *data, void *dataptr);

    void ViewpointRender(X3dNode *data, void*);

    void draw();

    void init();

    void finalize();

}

#endif // LIBCPPRWD_H_INCLUDED
