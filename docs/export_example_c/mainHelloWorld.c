#include <stdio.h>
#include "X3dSceneGraph.h"

/* 
   matching white_dune commandline usage:

	dune -prefix Pre_ -c file1.x3dv 
	dune -prefix Pre_ +c Append_ file2.x3dv 

*/

void myTextDoWithData(Pre_Node *self, void *data) {
    struct Pre_Text* text = (struct Pre_Text *) self;
    if (text)
        if (text->string_length > 0)
            printf("%s\n", text->string[0]);
}

void myAnchorTreeDoWithData(Pre_Node *self, void *data) {
    struct Pre_Anchor* anchor = (struct Pre_Anchor *) self;
    if (anchor)
        printf("Anchor node has %d children\n", anchor->children_length);
}

int main(int argc, char** argv) {
    struct Pre_SceneGraph sceneGraph;
    struct Append_Pre_SceneGraph appendedSceneGraph;

    Pre_TextDoWithDataCallback = &myTextDoWithData;
    Pre_AnchorTreeDoWithDataCallback = &myAnchorTreeDoWithData;

    Pre_SceneGraphInit(&sceneGraph);
    Pre_GroupTreeDoWithData(&sceneGraph.root, NULL);

    Append_Pre_SceneGraphInit(&appendedSceneGraph);
    Pre_GroupTreeDoWithData(&appendedSceneGraph.root, NULL);
    return 0;
}
