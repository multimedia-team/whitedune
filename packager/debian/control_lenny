Source: whitedune
Section: graphics
Priority: optional
Maintainer: J. "MUFTI" Scheurich <mufti@csv.ica.uni-stuttgart.de>
Build-Depends: debhelper (>= 5), make, bison, flex, gawk, m4, x11proto-core-dev, libexpat1-dev, libxmu-dev, libxi-dev, x-dev, libxi-dev, libgl-dev|libgl1-mesa-dev, libglu1-xorg-dev|libglu1-mesa-dev, libmotif-dev|lesstif-dev|lesstif2-dev, libpng-dev|libpng12-dev, libjpeg62-dev, g++, gcc, libusb-dev
Standards-Version: 3.5.6.1

Package: whitedune
Architecture: any
Depends: ${shlibs:Depends}, gimp, kolourpaint, audacity, rcs
Suggests: vrwave, openvrml-lookat, freewrl, moonlight, admesh, wings3d
Description: Graphical X3D/VRML97 editor, simple NURBS/Superformula 3D modeller and  animation tool
 VRML97 (Virtual Reality Modelling Language) is the ISO standard for
 displaying 3D data over the web. It has support for animation, realtime
 interaction and multimedia (image, movie, sound).  
 VRML97 can be written by popular programs like maya, catia, 3D Studio MAX, 
 cinema4D, blender, wings3d and others, but (unlike white_dune) most of this 
 programs support only a small part of the VRML97 features.
 X3DV (VRML encoded X3D) is the direct successor of VRML97. 
 X3DV can be converted from/to XML encoded X3D without loss (different syntax 
 but same content).
 White_dune can read X3DV/VRML97 files, display and let the user change the 
 scenegraph and all fields of all nodes. 
 Some documentation how to use white_dune is included.
 The most powerful 3D modelling features of white_dune support the 
 VRML97 Amendment1 style/X3D NURBS nodes and Superformula based PROTOs. 
 This is mainly convenient for building some rounded shapes and for beginners.
 For artistic mesh modelling work, the usage of a static 3D modeller with 
 VRML97 export features may be useful. The result can be either used unchanged
 in a bigger VRML97 file via the Inline VRML97 command without breaking the 
 toolchain or improved in white_dune.
 Examples for free (speech) static 3D modellers with VRML97 export available 
 under Linux are blender, wings3d, art of illusion or sharp construct.
 Under Linux, white_dune support some 3D inputdevices like joysticks, gamepads
 or all devices supported via the Xinput protocol.
 White_dune support quadbuffer stereo visuals. Under Linux, this can be used
 with Elsa Revelator or Crystal eyes shutterglasses and special drivers for 
 special graphiccards like Nvidia Quadro or ATI FireGL 4.
 This version of white_dune support both english, german menus and messages. 
 It also support italian menus together with english messages.
 Information how to translate white_dune to foreign languages can be found under
 docs/developer_docs/dune_developer.html#localisation
