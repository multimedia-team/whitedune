/*
 * NodeGeoViewpoint.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeGeoViewpoint.h"
#include "Proto.h"
#include "MFVec3f.h"
#include "ExposedField.h"
#include "Field.h"
#include "RenderState.h"
#include "DuneApp.h"
#include "Util.h"
#include "Vec3f.h"
#include "Scene.h"
#include "NodeIndexedFaceSet.h"
#include "NodeIndexedLineSet.h"
#include "NodePointSet.h"

ProtoGeoViewpoint::ProtoGeoViewpoint(Scene *scene)
  : GeoProto(scene, "GeoViewpoint")
{
    set_bind.set(addEventIn(SFBOOL, "set_bind"));
    // problem: Field for VRML97, ExposedField for X3D 8-(
    description.set(
          addField(SFSTRING, "description", new SFString("")));
    fieldOfView.set(
          addExposedField(SFFLOAT, "fieldOfView", new SFFloat(0.785398f), 
                          new SFFloat(0.0f), new SFFloat(M_PI)));
    headlight.set(
          addExposedField(SFBOOL, "headlight", new SFBool(true)));
    jump.set(
         addExposedField(SFBOOL, "jump", new SFBool(true)));
    static const char *types[] = { "ANY", "WALK", "EXAMINE", "FLY", "NONE", 
                                   NULL};
    StringArray *defaulttypes = new StringArray();
    defaulttypes->append("WALK");
    defaulttypes->append("ANY");
    navType.set(
          addExposedField(MFSTRING, "navType", new MFString(defaulttypes), 0, 
                          types));
    setFieldFlags(navType, FF_VRML_ONLY);
    static const char *typesX3d[] = { "ANY", "WALK", "EXAMINE", "FLY", "NONE",
                                      "LOOKAT",NULL};
    StringArray *defaulttypesX3D = new StringArray();
    defaulttypesX3D->append("WALK");
    defaulttypesX3D->append("EXAMINE");
    defaulttypesX3D->append("ANY");
    navTypeX3D.set(
          addExposedField(MFSTRING, "navType", new MFString(defaulttypesX3D), 
                          0, typesX3d));
    setFieldFlags(navTypeX3D, FF_X3D_ONLY);
    // why is orientation and position a field and not a exposedField ?
    orientation.set(
         addField(SFROTATION, "orientation", 
                         new SFRotation(0, 0, 1, 0)));

    position.set(
          addExposedField(SFSTRING, "position", new SFString("0 0 100000")));
    setFieldFlags(position, FF_VRML_ONLY);
    positionX3D.set(
         addField(SFVEC3D, "position", new SFVec3d(0, 0, 100000)));
    setFieldFlags(positionX3D, FF_X3D_ONLY);

    speedFactor.set(
          addField(SFFLOAT, "speedFactor", new SFFloat(1.0f), 
                   new SFFloat(0.0f)));

    addEventOut(SFTIME, "bindTime");
    addEventOut(SFBOOL, "isBound");
}

Node *
ProtoGeoViewpoint::create(Scene *scene)
{ 
    return new NodeGeoViewpoint(scene, this); 
}

NodeGeoViewpoint::NodeGeoViewpoint(Scene *scene, Proto *def)
  : GeoNode(scene, def)
{
}

void
NodeGeoViewpoint::setField(int index, FieldValue *value)
{
    if (index == position_Field()) {
        SFVec3d *value3d = new SFVec3d((SFString *)value);
        Node::setField(positionX3D_Field(), value3d);
    }
    Node::setField(index, value);
    update();
}

Node *
NodeGeoViewpoint::convert2Vrml(void) 
{
    const double *values = positionX3D()->getValue();
    char buf[4096];
    mysnprintf(buf, 4095, "%g %g %g", values[0], values[1], values[2]);
    SFString *string = new SFString(buf);
    position(string);    
    return NULL;
}


