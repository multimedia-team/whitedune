/*
 * OutputApp.h
 *
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _OUTPUT_APP_H
#define _OUTPUT_APP_H

extern bool parseCommandlineArgumentOutput(int & i,int argc, char** argv);

enum {
    MATERIAL_NAME_BEFORE_LAST_UNDERSCORE = 1,
    MATERIAL_NAME_AFTER_FIRST_UNDERSCORE = 2
};

class WonderlandModuleExport;

class OutputApp {
public:
                        OutputApp();

    void                SetKeepURLs(bool flag) { _keepURLs = flag; }
    bool                GetKeepURLs() const { return _keepURLs; }        

    void                SetIndent(int value) {_indent = value;}
    int                 GetIndent(void) {return _indent;}

    void                SetFloatDigits(int value);
    int                 GetFloatDigits(void) {return _floatDigits;}

    void                forbidEFloatWriteFormat();
    void                enableEFloatWriteFormat(int writeFlags);
    void                disableEFloatWriteFormat(int writeFlags);
    void                useEFloatWriteFormat(int flag);

    bool                GetIncludeProtos() const { return _includeProtos; }
    void                SetIncludeProtos(bool flag) { _includeProtos = flag; }

    bool                GetkrFormating() const { return _krFormating; }
    void                SetkrFormating(bool flag) { _krFormating = flag; }

    bool                GetCompress() const { return _compress; }
    void                SetCompress(bool flag) { _compress = flag; }

    bool                GetNormalsOnPureVRML97() const 
                           { return _normalsOnPureVRML97; }        
    void                SetNormalsOnPureVRML97(bool flag) 
                           { _normalsOnPureVRML97 = flag; }

    int                 writeWonderlandModule(const char* directory, 
                                              const char* name, Scene* scene,
                                              bool manyClasses);
    bool                isWonderlandModuleExport(void) 
                           { return _wonderlandModuleExport != NULL; }
    int                 writeWonderlandModuleArtPath(int filedes,  
                                                     const char* fileName);

    char               *GetWonderlandModuleExportPath(void)
                           { return _wonderlandModuleExportPath;}
    void                SetWonderlandModuleExportPath(char *path)
                           {
                           if (_wonderlandModuleExportPath != NULL)
                               free(_wonderlandModuleExportPath);
                           _wonderlandModuleExportPath = strdup(path);
                           }
    char               *GetWonderlandModuleExportPrefix(void)
                           { return _wonderlandModuleExportPrefix;}
    void                SetWonderlandModuleExportPrefix(char *prefix);
    bool                GetWonderlandModuleExportManyClasses(void)
                           { return _wonderlandModuleExportManyClasses;}
    void                SetWonderlandModuleExportManyClasses(bool flag)
                           { _wonderlandModuleExportManyClasses = flag; }

    void                SetWrittenElementsPerJavaArray(int numberElements) 
                           { _writtenElementsPerJavaArray = numberElements; }
    int                 GetWrittenElementsPerJavaArray(void)
                           { return _writtenElementsPerJavaArray; }


    char               *GetCattExportPath(void)
                           { return _cattExportPath;}
    void                SetCattExportPath(char *path)
                           {
                           if (_cattExportPath != NULL)
                               free(_cattExportPath);
                           _cattExportPath = strdup(path);
                           }

    void                OutputSetDefaults();

    void                OutputLoadPreferences();
    void                OutputSavePreferences();

    bool                SkipMaterialNameBeforeFirstUnderscore(void)
                           { return _skipMaterialNameBeforeFirstUnderscore; }
    bool                SetSkipMaterialNameBeforeFirstUnderscore(bool f)
                           { return _skipMaterialNameBeforeFirstUnderscore = f;}
    bool                SkipMaterialNameAfterLastUnderscore(void)
                           { return _skipMaterialNameAfterLastUnderscore; }
    bool                SetSkipMaterialNameAfterLastUnderscore(bool f)
                           { return _skipMaterialNameAfterLastUnderscore = f;}

    bool                IsMaterialNameBeforeLastUnderscore(void)
                           { return _materialNameGeneration & 
                                    MATERIAL_NAME_BEFORE_LAST_UNDERSCORE; }
    bool                IsMaterialNameAfterFirstUnderscore(void)
                           { return _materialNameGeneration & 
                                    MATERIAL_NAME_AFTER_FIRST_UNDERSCORE; }

    bool                GetCattExportSrcRec(void) { return _cattExportSrcRec; }
    void                SetCattExportSrcRec(bool flag) 
                           { _cattExportSrcRec = flag; }

    bool                GetRevisionControlCheckinFlag(void) 
                           { return _revisionControlCheckinFlag; }
    void                SetRevisionControlCheckinFlag(bool flag) 
                           { _revisionControlCheckinFlag = flag; }

    const char         *GetRevisionControlCheckinCommand(void) 
                           { return _revisionControlCheckinCommand; }
    void                SetRevisionControlCheckinCommand(const char* command) 
                           {
                           if (_revisionControlCheckinCommand != NULL)
                              free(_revisionControlCheckinCommand); 
                           _revisionControlCheckinCommand = strdup(command); 
                           }

    const char          *GetDefaultAc3dMaterialName(void) 
                           { return _defaultAc3dMaterialName; }
    void                 SetDefaultAc3dMaterialName(const char *name) 
                           { 
                           if (_defaultAc3dMaterialName != NULL)
                              free(_defaultAc3dMaterialName); 
                           _defaultAc3dMaterialName = strdup(name); 
                           }

    bool                GetAc3dExport4Raven(void) { return _ac3dExport4Raven; }
    void                SetAc3dExport4Raven(bool b) { _ac3dExport4Raven = b; }

    bool                GetAc3dExportConvert2Gif(void) 
#ifdef HAVE_IMAGE_CONVERTER
                           { return _ac3dExportConvert2Gif; }
#else
                           { return false; }
#endif
    void                SetAc3dExportConvert2Gif(bool b) 
                           { _ac3dExportConvert2Gif = b; }

    const char          *GetDefaultCattMaterialName(void) 
                           { return _defaultCattMaterialName; }
    void                 SetDefaultCattMaterialName(const char *name) 
                           { 
                           if (_defaultCattMaterialName != NULL)
                              free(_defaultCattMaterialName); 
                           _defaultCattMaterialName = strdup(name); 
                           }
                         
private:
    bool                    _keepURLs;
    int                     _indent;
    int                     _floatDigits;
    int                     _oldFloatDigits;
    bool                    _useEFloatWriteFormat;
    bool                    _krFormating;
    bool                    _includeProtos;
    bool                    _compress;    
    bool                    _normalsOnPureVRML97;
    bool                    _skipMaterialNameBeforeFirstUnderscore;
    bool                    _skipMaterialNameAfterLastUnderscore;
    int                     _materialNameGeneration;
    char                   *_defaultAc3dMaterialName;
    bool                    _ac3dExport4Raven;
    bool                    _ac3dExportConvert2Gif;
    char                   *_defaultCattMaterialName;
    bool                    _cattExportSrcRec;
    bool                    _revisionControlCheckinFlag;
    char                   *_revisionControlCheckinCommand;
    char                   *_x3dv2X3dCommand;
    WonderlandModuleExport *_wonderlandModuleExport;
    char                   *_wonderlandModuleExportPath;
    char                   *_wonderlandModuleExportPrefix;
    bool                    _wonderlandModuleExportManyClasses;
    int                     _writtenElementsPerJavaArray;
    char                   *_cattExportPath;
};

#endif

