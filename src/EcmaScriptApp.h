/*
 * EcmaScriptApp.h
 *
 * Copyright (C) 1999 Stephen F. White, 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _ECMASCRIPT_APP_H
#define _ECMASCRIPT_APP_H

class EcmaScriptApp {
public:
                        EcmaScriptApp();
    void                EcmaScriptLoadPreferences();
    bool                GetEcmaScriptAddInitialise() const 
                              { return _ecmaScriptAddInitialise; }
    bool                GetEcmaScriptAddShutdown() const 
                              { return _ecmaScriptAddShutdown; }
    bool                GetEcmaScriptAddEventsProcessed() const 
                              { return _ecmaScriptAddEventsProcessed; }
    bool                GetEcmaScriptAddAvailableFunctions() const 
                              { return _ecmaScriptAddAvailableFunctions; }
    bool                GetEcmaScriptAddAllowedValues() const 
                              { return _ecmaScriptAddAllowedValues; }
    bool                GetEcmaScriptAddAllowedComponents() const 
                              { return _ecmaScriptAddAllowedComponents; }
    bool                GetEcmaScriptAddExampleUsage() const 
                              { return _ecmaScriptAddExampleUsage; }

    bool                GetEcmaScriptAddMathObject() const
                              { return _ecmaScriptAddMathObject; }
    bool                GetEcmaScriptAddBrowserObject() const
                              { return _ecmaScriptAddBrowserObject; }

    void                SetEcmaScriptAddInitialise(bool s)
                              { _ecmaScriptAddInitialise = s; }
    void                SetEcmaScriptAddShutdown(bool s)
                              { _ecmaScriptAddShutdown = s; }
    void                SetEcmaScriptAddEventsProcessed(bool s)
                              { _ecmaScriptAddEventsProcessed = s; }
    void                SetEcmaScriptAddAvailableFunctions(bool s)
                              { _ecmaScriptAddAvailableFunctions =s; }
    void                SetEcmaScriptAddAllowedValues(bool s)
                              { _ecmaScriptAddAllowedValues = s; }
    void                SetEcmaScriptAddAllowedComponents(bool s)
                              { _ecmaScriptAddAllowedComponents = s; }
    void                SetEcmaScriptAddExampleUsage(bool s)
                              { _ecmaScriptAddExampleUsage = s; }

    void                SetEcmaScriptAddMathObject(bool s)
                              { _ecmaScriptAddMathObject = s; }
    void                SetEcmaScriptAddBrowserObject(bool s)
                              { _ecmaScriptAddBrowserObject = s; }
    void                EcmaScriptSavePreferences(void);


private: 
    bool                _ecmaScriptAddInitialise;
    bool                _ecmaScriptAddShutdown;
    bool                _ecmaScriptAddEventsProcessed;
    bool                _ecmaScriptAddAvailableFunctions;
    bool                _ecmaScriptAddAllowedValues;
    bool                _ecmaScriptAddAllowedComponents;
    bool                _ecmaScriptAddExampleUsage;
    bool                _ecmaScriptAddMathObject;
    bool                _ecmaScriptAddBrowserObject;
};

#endif
