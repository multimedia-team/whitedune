/*
 * NodeNurbsGroup.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#ifndef FLT_MAX
# include <float.h>
#endif
#include "stdafx.h"

#include "NodeNurbsGroup.h"
#include "Proto.h"
#include "Field.h"
#include "FieldValue.h"
#include "MFNode.h"
#include "SFVec3f.h"
#include "SFFloat.h"
#include "DuneApp.h"
#include "NodeNurbsSurface.h"
#include "NodeShape.h"
#include "Scene.h"
#include "NodeNurbsSet.h"

ProtoNurbsGroup::ProtoNurbsGroup(Scene *scene)
  : ProtoGroup(scene, "NurbsGroup")
{
    tessellationScale.set(
          addExposedField(SFFLOAT, "tessellationScale", new SFFloat(1.0f), 
                          new SFFloat(0.0f), new SFFloat(FLT_MAX)));
#ifdef HAVE_NURBSSET_LIKE_CHILDREN
    getField(children)->addToNodeType(PARAMETRIC_GEOMETRY_NODE);
#endif

}

Node *
ProtoNurbsGroup::create(Scene *scene)
{ 
    return new NodeNurbsGroup(scene, this); 
}

NodeNurbsGroup::NodeNurbsGroup(Scene *scene, Proto *def)
  : NodeGroup(scene, def)
{
}

void
NodeNurbsGroup::draw(void)
{
    int i;
    NodeList *childList = children()->getValues();

    glPushName(children_Field());  // field offset

    for (i = 0; i < childList->size(); i++)
        childList->get(i)->bind();

    glPushName(0);
    for (i = 0; i < childList->size(); i++) {
        glLoadName(i);
        childList->get(i)->draw();
    }
    glPopName();

    for (i = 0; i < childList->size(); i++)
        childList->get(i)->unbind();

    glPopName();
}

int
NodeNurbsGroup::writeProto(int f)
{
    RET_ONERROR( mywritestr(f ,"EXTERNPROTO NurbsGroup[\n") )    
    TheApp->incSelectionLinenumber();
    RET_ONERROR( writeProtoArguments(f) )
    RET_ONERROR( mywritestr(f ," ]\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ,"[\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:web3d:vrml97:node:NurbsGroup\",\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:inet:blaxxun.com:node:NurbsGroup\",\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:ParaGraph:NurbsGROUP\",\n") )
    TheApp->incSelectionLinenumber();
#ifdef HAVE_VRML97_AMENDMENT1_PROTO_URL
    RET_ONERROR( mywritestr(f ," \"") )
    RET_ONERROR( mywritestr(f ,HAVE_VRML97_AMENDMENT1_PROTO_URL) )
    RET_ONERROR( mywritestr(f ,"/NurbsGroupPROTO.wrl") )
    RET_ONERROR( mywritestr(f ,"\"\n") )
    TheApp->incSelectionLinenumber();
#else
    RET_ONERROR( mywritestr(f ," \"NurbsGroupPROTO.wrl\",\n") )
    TheApp->incSelectionLinenumber();
#endif
    RET_ONERROR( mywritestr(f ," \"http://129.69.35.12/dune/docs/vrml97Amendment1/NurbsGroupPROTO.wrl\"\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ,"]\n") )
    TheApp->incSelectionLinenumber();
    return 0;
}

   
int             
NodeNurbsGroup::write(int filedes, int indent) 
{
    if (_scene->isPureVRML()) {
        NodeGroup group(this);
        RET_ONERROR( group.write(filedes, indent) )
    } else
        RET_ONERROR( NodeData::write(filedes, indent) )
    return 0;
}

void
NodeNurbsGroup::setHandle(Node* caller, float newWeight,
                          const Vec3f &newV, const Vec3f &oldV)
{
    int i;
    NodeList   *childList = children()->getValues();
    for (i = 0; i < childList->size(); i++) {
        Node *child = childList->get(i);
        if (child->getType() == VRML_NURBS_SURFACE) {
            if (child != caller)
                ((NodeNurbsSurface *)child)->setHandle(newWeight, newV, oldV);
        } else if (child->getType() == VRML_SHAPE) {
            Node *node = ((NodeShape *)child)->geometry()->getValue();
            if ((node != caller) && (node->getType() == VRML_NURBS_SURFACE))
                ((NodeNurbsSurface *)node)->setHandle(newWeight, newV, oldV);
        }
    }
}

void
NodeNurbsGroup::backupFieldsAppend(Node* caller, int field)
{
    int i;
    NodeList   *childList = children()->getValues();
    for (i = 0; i < childList->size(); i++) {
        Node *child = childList->get(i);
        if (child->getType() == VRML_NURBS_SURFACE) {
            if (child != caller)
                _scene->backupFieldsAppend(child, field);
        } else if (child->getType() == VRML_SHAPE) {
            Node *node = ((NodeShape *)child)->geometry()->getValue();
            if ((node != caller) && (node->getType() == VRML_NURBS_SURFACE))
                _scene->backupFieldsAppend(node, field);
        }
    }
}

Node *
NodeNurbsGroup::convert2X3d(void)
{
    NodeList *childs = children()->getValues();
    NodeNurbsSet *nurbsSet = (NodeNurbsSet *)_scene->createNode("NurbsSet");
    
    nurbsSet->tessellationScale(new SFFloat(*tessellationScale()));
    _scene->addNodes(nurbsSet, nurbsSet->geometry_Field(), childs);
    _scene->changeRoutes(nurbsSet, nurbsSet->geometry_Field(),
                         this, this->children_Field(), true);
    _scene->changeRoutes(nurbsSet, nurbsSet->tessellationScale_Field(),
                         this, this->tessellationScale_Field(), true);
    _scene->UpdateViews(NULL, UPDATE_REDRAW, NULL);
    children()->convert2X3d();
    return nurbsSet;
}

Node *
NodeNurbsGroup::convert2Vrml(void) 
{
    NodeGroup *group = (NodeGroup *)_scene->createNode("Group");
    NodeList *newChildren = buildVrml97Children(children());

    _scene->addNodes(group, group->children_Field(), newChildren);
    _scene->changeRoutes(group, group->children_Field(),
                         this, this->children_Field(), true);
    _scene->UpdateViews(NULL, UPDATE_REDRAW, NULL);
    return group;
}
