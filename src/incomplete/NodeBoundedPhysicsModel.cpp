/*
 * NodeBoundedPhysicsModel.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"
#include <sys/types.h>
#include <sys/stat.h>
#ifndef WIN32
# include <fcntl.h>
#endif

#include "NodeBoundedPhysicsModel.h"
#include "SFBool.h"
#include "SFNode.h"

ProtoBoundedPhysicsModel::ProtoBoundedPhysicsModel(Scene *scene) 
  : BoundedPhysicsModelProto(scene, "BoundedPhysicsModel")
{                    
    enabled.set (
           addExposedField(SFBool, "enabled", new SFBool(true))); 
    geometry.set (
           addExposedField(SFNODE, "geometry", new SFNode(), 
                           GEOMETRY_NODE));
}

Node *
ProtoBoundedPhysicsModel::create(Scene *scene)
{ 
    return new NodeBoundedPhysicsModel(scene, this); 
}

NodeBoundedPhysicsModel::NodeBoundedPhysicsModel(Scene *scene, Proto *def)
  : Node(scene, def)
{
}

NodeBoundedPhysicsModel::~NodeBoundedPhysicsModel()
{
}

int NodeBoundedPhysicsModel::getProfile(void) const
{ 
    return PROFILE_FULL; 
}

