/*
 * NodeSpotLight.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeSpotLight.h"
#include "Scene.h"
#include "Proto.h"
#include "FieldValue.h"
#include "MFFloat.h"
#include "SFVec3f.h"
#include "SFColor.h"
#include "SFFloat.h"
#include "SFBool.h"


ProtoSpotLight::ProtoSpotLight(Scene *scene)
  : Proto(scene, "SpotLight")
{
    ambientIntensity.set(
          addExposedField(SFFLOAT, "ambientIntensity", new SFFloat(0.0f), 
                          new SFFloat(0.0f), new SFFloat(1.0f)));
    attenuation.set(
          addExposedField(SFVEC3F, "attenuation", new SFVec3f(1.0f, 0.0f, 0.0f),
                          new SFFloat(0.0f)));
    beamWidth.set(
          addExposedField(SFFLOAT, "beamWidth", new SFFloat(1.570796f), 
                          new SFFloat(0.0f), new SFFloat(M_PI/2.0f)));
    color.set(
          addExposedField(SFCOLOR, "color", new SFColor(1.0f, 1.0f, 1.0f)));
    cutOffAngle.set(
          addExposedField(SFFLOAT, "cutOffAngle", new SFFloat(0.785398f), 
                          new SFFloat(0.0f), new SFFloat(M_PI/2.0f)));
    direction.set(
          addExposedField(SFVEC3F, "direction", 
                          new SFVec3f(0.0f, 0.0f, -1.0f)));
    global.set(
          addExposedField(SFBOOL, "global", new SFBool(false)));
    setFieldFlags(global, FF_X3D_ONLY);

    intensity.set(
          addExposedField(SFFLOAT, "intensity", new SFFloat(1.0f), 
                          new SFFloat(0.0f), new SFFloat(1.0f)));
    location.set(
          addExposedField(SFVEC3F, "location", new SFVec3f(0.0f, 0.0f, 0.0f)));
    on.set(
          addExposedField(SFBOOL, "on", new SFBool(true)));
    radius.set(
          addExposedField(SFFLOAT, "radius", new SFFloat(100.0f), 
                          new SFFloat(0.0f)));
    kambiLightCommonFields()
    projectionAngle.set(
          addExposedField(SFFLOAT, "projectionAngle", new SFFloat(0.0f)));
    setFieldFlags(projectionAngle, FF_KAMBI_ONLY);
}

Node *
ProtoSpotLight::create(Scene *scene)
{ 
    return new NodeSpotLight(scene, this); 
}

NodeSpotLight::NodeSpotLight(Scene *scene, Proto *def)
  : Node(scene, def)
{
}

void
NodeSpotLight::preDraw()
{
//    float fbeamWidth = beamWidth()->getValue();
//    float fradius = radius()->getValue();

    if (on()->getValue()) {
        float ambientColor[4], diffuseColor[4];
        float pos[4];
        for (int i = 0; i < 3; i++) {
            ambientColor[i] = color()->getValue()[i] * 
                              ambientIntensity()->getValue();
            diffuseColor[i] = color()->getValue()[i] * 
                              intensity()->getValue();
            pos[i] = location()->getValue()[i];
        }
        ambientColor[3] = diffuseColor[3] = 1.0f;
        pos[3] = 1.0f;

        _light = (GLenum) _scene->allocateLight();

        glLightfv(_light, GL_AMBIENT, ambientColor);
        glLightfv(_light, GL_DIFFUSE, diffuseColor);
        glLightfv(_light, GL_POSITION, pos);
        glLightfv(_light, GL_SPECULAR, diffuseColor);
        glLightfv(_light, GL_SPOT_DIRECTION, direction()->getValue());
        glLightf(_light, GL_SPOT_CUTOFF, RAD2DEG(cutOffAngle()->getValue()));
        glLightf(_light, GL_SPOT_EXPONENT, 0.0f);
        glLightf(_light, GL_CONSTANT_ATTENUATION, attenuation()->getValue()[0]);
        glLightf(_light, GL_LINEAR_ATTENUATION, attenuation()->getValue()[1]);
        glLightf(_light, GL_QUADRATIC_ATTENUATION, attenuation()->getValue()[2]);
        glEnable(_light);
    }
}

int NodeSpotLight::getProfile(void) const
{ 
    return PROFILE_INTERACTIVE;
}

const char* 
NodeSpotLight::getComponentName(void) const
{
    static const char* name = "Lighting";
    return name;
}

int         
NodeSpotLight::getComponentLevel(void) const
{
    if (!isDefault(radius_Field()))
        return 3;
    if (!isDefault(beamWidth_Field()))
        return 3;
    if (attenuation()->getValue()[2] != 0)
        return 3;
    return -1;
}


