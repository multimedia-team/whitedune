/*
 * SceneProtoMap.h
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"
#include "SceneProtoMap.h"

#include "NodeAnchor.h"
#include "NodeAppearance.h"
#include "NodeArc2D.h"
#include "NodeArcClose2D.h"
#include "NodeARSensor.h"
#include "NodeAudioClip.h"
#include "NodeBackground.h"
#include "NodeBallJoint.h"
#include "NodeBillboard.h"
#include "NodeBlendMode.h"
#include "NodeBooleanFilter.h"
#include "NodeBooleanToggle.h"
#include "NodeBooleanTrigger.h"
#include "NodeBooleanSequencer.h"
#include "NodeBoundedPhysicsModel.h"
#include "NodeBox.h"
#include "NodeCADAssembly.h"
#include "NodeCADFace.h"
#include "NodeCADLayer.h"
#include "NodeCADPart.h"
#include "NodeCOVER.h"
#include "NodeCattExportRec.h"
#include "NodeCattExportSrc.h"
#include "NodeCircle2D.h"
#include "NodeClipPlane.h"
#include "NodeCollidableOffset.h"
#include "NodeCollidableShape.h"
#include "NodeCollision.h"
#include "NodeCollisionCollection.h"
#include "NodeCollisionSensor.h"
#include "NodeCollisionSpace.h"
#include "NodeColor.h"
#include "NodeColorDamper.h"
#include "NodeColorInterpolator.h"
#include "NodeColorRGBA.h"
#include "NodeComposedCubeMapTexture.h"
#include "NodeComposedShader.h"
#include "NodeComposedTexture3D.h"
#include "NodeCone.h"
#include "NodeConeEmitter.h"
#include "NodeContact.h"
#include "NodeContour2D.h"
#include "NodeContourPolyline2D.h"
#include "NodeCoordinate.h"
#include "NodeCoordinateDamper.h"
#include "NodeCoordinateDeformer.h"
#include "NodeCoordinateDouble.h"
#include "NodeCoordinateInterpolator2D.h"
#include "NodeCoordinateInterpolator.h"
#include "NodeCubeTexture.h"
#include "NodeCylinder.h"
#include "NodeCylinderSensor.h"
#include "NodeDirectionalLight.h"
#include "NodeDISEntityManager.h"
#include "NodeDISEntityTypeMapping.h"
#include "NodeDisk2D.h"
#include "NodeDoubleAxisHingeJoint.h"
#include "NodeEaseInEaseOut.h"
#include "NodeElevationGrid.h"
#include "NodeEspduTransform.h"
#include "NodeExplosionEmitter.h"
#include "NodeExtrusion.h"
#include "NodeFillProperties.h"
#include "NodeFloatVertexAttribute.h"
#include "NodeFog.h"
#include "NodeFogCoordinate.h"
#include "NodeFontStyle.h"
#include "NodeForcePhysicsModel.h"
#include "NodeGeneratedCubeMapTexture.h"
#include "NodeGeneratedShadowMap.h"
#include "NodeGeoCoordinate.h"
#include "NodeGeoElevationGrid.h"
#include "NodeGeoMetadata.h"
#include "NodeGeoLocation.h"
#include "NodeGeoLOD.h"
#include "NodeGeoPositionInterpolator.h"
#include "NodeGeoProximitySensor.h"
#include "NodeGeoOrigin.h"
#include "NodeGeoTouchSensor.h"
#include "NodeGeoTransform.h"
#include "NodeGeoViewpoint.h" 
#include "NodeGravityPhysicsModel.h"
#include "NodeGroup.h"
#include "NodeHAnimDisplacer.h"
#include "NodeHAnimHumanoid.h"
#include "NodeHAnimJoint.h"
#include "NodeHAnimSegment.h"
#include "NodeHAnimSite.h"
#include "NodeImageCubeMapTexture.h"
#include "NodeImageTexture.h"
#include "NodeImageTexture3D.h"
#include "NodeIndexedFaceSet.h"
#include "NodeIndexedLineSet.h"
#include "NodeIndexedQuadSet.h"
#include "NodeIndexedTriangleFanSet.h"
#include "NodeIndexedTriangleSet.h"
#include "NodeIndexedTriangleStripSet.h"
#include "NodeInline.h"
#include "NodeInlineLoadControl.h"
#include "NodeIntegerSequencer.h"
#include "NodeIntegerTrigger.h"
#include "NodeJoystickSensor.h"
#include "NodeKambiAppearance.h"
#include "NodeKambiHeadLight.h"
#include "NodeKambiInline.h"
#include "NodeKambiNavigationInfo.h"
#include "NodeKambiOctreeProperties.h"
#include "NodeKambiTriangulation.h"
#include "NodeKeySensor.h"
#include "NodeLabView.h"
#include "NodeLayer.h"
#include "NodeLayerSet.h"
#include "NodeLayout.h"
#include "NodeLayoutGroup.h"
#include "NodeLayoutLayer.h"
#include "NodeLdrawDatExport.h"
#include "NodeLinePickSensor.h"
#include "NodeLineProperties.h"
#include "NodeLineSet.h"
#include "NodeLoadSensor.h"
#include "NodeLocalFog.h"
#include "NodeLOD.h"
#include "NodeMaterial.h"
#include "NodeMatrix3VertexAttribute.h"
#include "NodeMatrix4VertexAttribute.h"
#include "NodeMatrixTransform.h"
#include "NodeMetadataDouble.h"
#include "NodeMetadataFloat.h"
#include "NodeMetadataInteger.h"
#include "NodeMetadataSet.h"
#include "NodeMetadataString.h"
#include "NodeMotorJoint.h"
#include "NodeMovieTexture.h"
#include "NodeMultiGeneratedTextureCoordinate.h"
#include "NodeMultiTexture.h"
#include "NodeMultiTextureCoordinate.h"
#include "NodeMultiTextureTransform.h"
#include "NodeNavigationInfo.h"
#include "NodeNormal.h"
#include "NodeNormalInterpolator.h"
#include "NodeNurbsCurve.h"
#include "NodeNurbsCurve2D.h"
#include "NodeNurbsGroup.h"
#include "NodeNurbsOrientationInterpolator.h"
#include "NodeNurbsPositionInterpolator.h"
#include "NodeNurbsSet.h"
#include "NodeNurbsSurface.h"
#include "NodeNurbsSurfaceInterpolator.h"
#include "NodeNurbsSweptSurface.h"
#include "NodeNurbsSwungSurface.h"
#include "NodeNurbsTextureSurface.h"
#include "NodeNurbsTextureCoordinate.h"
#include "NodeNurbsTrimmedSurface.h"
#include "NodeOdeMotorJoint.h"
#include "NodeOdeSingleAxisHingeJoint.h"
#include "NodeOdeSliderJoint.h"
#include "NodeOrientationChaser.h"
#include "NodeOrientationDamper.h"
#include "NodeOrientationInterpolator.h"
#include "NodeOrthoViewpoint.h"
#include "NodePackagedShader.h"
#include "NodeParticleSystem.h"
#include "NodePickableGroup.h"
#include "NodePixelTexture.h"
#include "NodePixelTexture3D.h"
#include "NodePlaneSensor.h"
#include "NodePointEmitter.h"
#include "NodePointLight.h"
#include "NodePointPickSensor.h"
#include "NodePointSet.h"
#include "NodePolyline2D.h"
#include "NodePolylineEmitter.h"
#include "NodePolypoint2D.h"
#include "NodePositionChaser.h"
#include "NodePositionChaser2D.h"
#include "NodePositionDamper.h"
#include "NodePositionDamper2D.h"
#include "NodePositionInterpolator.h"
#include "NodePositionInterpolator2D.h"
#include "NodePrimitivePickSensor.h"
#include "NodeProgramShader.h"
#include "NodeProjectedTextureCoordinate.h"
#include "NodeProximitySensor.h"
#include "NodeQuadSet.h"
#include "NodeReceiverPdu.h"
#include "NodeRectangle2D.h"
#include "NodeRenderedTexture.h"
#include "NodeRigidBody.h"
#include "NodeRigidBodyCollection.h"
#include "NodeScalarChaser.h"
#include "NodeScalarInterpolator.h"
#include "NodeScreenFontStyle.h"
#include "NodeScreenGroup.h"
#include "NodeShaderPart.h"
#include "NodeShaderProgram.h"
#include "NodeShape.h"
#include "NodeSignalPdu.h"
#include "NodeSingleAxisHingeJoint.h"
#include "NodeSky.h"
#include "NodeSliderJoint.h"
#include "NodeSound.h"
#include "NodeSpaceSensor.h"
#include "NodeSphere.h"
#include "NodeSphereSensor.h"
#include "NodeSplinePositionInterpolator.h"
#include "NodeSplinePositionInterpolator2D.h"
#include "NodeSplineScalarInterpolator.h"
#include "NodeSpotLight.h"
#include "NodeSquadOrientationInterpolator.h"
#include "NodeStaticGroup.h"
#include "NodeSteeringWheel.h"
#include "NodeStringSensor.h"
#include "NodeSuperEllipsoid.h"
#include "NodeSuperExtrusion.h"
#include "NodeSuperRevolver.h"
#include "NodeSuperShape.h"
#include "NodeSurfaceEmitter.h"
#include "NodeSwitch.h"
#include "NodeTeapot.h"
#include "NodeTexCoordDamper.h"
#include "NodeTexCoordDamper2D.h"
#include "NodeText.h"
#include "NodeText3D.h"
#include "NodeTextureBackground.h"
#include "NodeTextureCoordinate.h"
#include "NodeTextureCoordinate3D.h"
#include "NodeTextureCoordinate4D.h"
#include "NodeTextureCoordinateGenerator.h"
#include "NodeTextureProperties.h"
#include "NodeTextureTransform.h"
#include "NodeTextureTransform3D.h"
#include "NodeTextureTransformMatrix3D.h"
#include "NodeTimeTrigger.h"
#include "NodeTimeSensor.h"
#include "NodeTouchSensor.h"
#include "NodeTransformSensor.h"
#include "NodeTransmitterPdu.h"
#include "NodeTriangleFanSet.h"
#include "NodeTriangleSet.h"
#include "NodeTriangleSet2D.h"
#include "NodeTriangleStripSet.h"
#include "NodeTrimmedSurface.h"
#include "NodeTransform.h"
#include "NodeTUIButton.h"
#include "NodeTUIComboBox.h"
#include "NodeTUIFloatSlider.h"
#include "NodeTUIFrame.h"
#include "NodeTUILabel.h"
#include "NodeTUIListBox.h"
#include "NodeTUIMap.h"
#include "NodeTUIProgressBar.h"
#include "NodeTUISlider.h"
#include "NodeTUISplitter.h"
#include "NodeTUITab.h"
#include "NodeTUITabFolder.h"
#include "NodeTUIToggleButton.h"
#include "NodeTwoSidedMaterial.h"
#include "NodeUniversalJoint.h"
#include "NodeVehicle.h"
#include "NodeViewpoint.h"
#include "NodeViewpointGroup.h"
#include "NodeViewport.h"
#include "NodeVisibilitySensor.h"
#include "NodeVirtualAcoustics.h"
#include "NodeVirtualSoundSource.h"
#include "NodeVolumeEmitter.h"
#include "NodeVolumePickSensor.h"
#include "NodeVrmlCut.h"
#include "NodeVrmlScene.h"
#include "NodeWave.h"
#include "NodeWindPhysicsModel.h"
#include "NodeWorldInfo.h"
#include "NodeComment.h"
#include "NodeImport.h"
#include "NodeExport.h"

void
SceneProtoMap::createProtoMap(ProtoMap *protos, Scene *scene)
{
    protos->add("Anchor", new ProtoAnchor(scene));
    protos->add("Appearance", new ProtoAppearance(scene));
    protos->add("Arc2D", new ProtoArc2D(scene));
    protos->add("ArcClose2D", new ProtoArcClose2D(scene));
    protos->add("ARSensor", new ProtoARSensor(scene));
    protos->add("AudioClip", new ProtoAudioClip(scene));
    protos->add("Background", new ProtoBackground(scene));
    protos->add("BallJoint", new ProtoBallJoint(scene));
    protos->add("Billboard", new ProtoBillboard(scene));
    protos->add("BlendMode", new ProtoBlendMode(scene));
    protos->add("BooleanFilter", new ProtoBooleanFilter(scene));
    protos->add("BooleanToggle", new ProtoBooleanToggle(scene));
    protos->add("BooleanTrigger", new ProtoBooleanTrigger(scene));
    protos->add("BooleanSequencer", new ProtoBooleanSequencer(scene));
    protos->add("BoundedPhysicsModel", new ProtoBoundedPhysicsModel(scene));
    protos->add("Box", new ProtoBox(scene));
    protos->add("CADAssembly", new ProtoCADAssembly(scene));
    protos->add("CADFace", new ProtoCADFace(scene));
    protos->add("CADLayer", new ProtoCADLayer(scene));
    protos->add("CADPart", new ProtoCADPart(scene));
    protos->add("CattExportRec", new ProtoCattExportRec(scene));
    protos->add("CattExportSrc", new ProtoCattExportSrc(scene));
    protos->add("Circle2D", new ProtoCircle2D(scene));
    protos->add("ClipPlane", new ProtoClipPlane(scene));
    protos->add("CollidableOffset", new ProtoCollidableOffset(scene));
    protos->add("CollidableShape", new ProtoCollidableShape(scene));
    protos->add("Collision", new ProtoCollision(scene));
    protos->add("CollisionCollection", new ProtoCollisionCollection(scene));
    protos->add("CollisionSensor", new ProtoCollisionSensor(scene));
    protos->add("CollisionSpace", new ProtoCollisionSpace(scene));
    protos->add("Color", new ProtoColor(scene));
    protos->add("ColorDamper", new ProtoColorDamper(scene));
    protos->add("ColorInterpolator", new ProtoColorInterpolator(scene));
    protos->add("ColorRGBA", new ProtoColorRGBA(scene));
    protos->add("ComposedCubeMapTexture", 
                new ProtoComposedCubeMapTexture(scene));
    protos->add("ComposedShader", new ProtoComposedShader(scene));
    protos->add("ComposedTexture3D", new ProtoComposedTexture3D(scene));
    protos->add("Cone", new ProtoCone(scene));
    protos->add("ConeEmitter", new ProtoConeEmitter(scene));
    protos->add("Contact", new ProtoContact(scene));
    protos->add("Contour2D", new ProtoContour2D(scene));
    protos->add("ContourPolyline2D", new ProtoContourPolyline2D(scene));
    protos->add("Coordinate", new ProtoCoordinate(scene));
    protos->add("CoordinateDamper", new ProtoCoordinateDamper(scene));
    protos->add("CoordinateDeformer", new ProtoCoordinateDeformer(scene));
    protos->add("CoordinateDouble", new ProtoCoordinateDouble(scene));
    protos->add("CoordinateInterpolator", 
                new ProtoCoordinateInterpolator(scene));
    protos->add("CoordinateInterpolator2D", 
                new ProtoCoordinateInterpolator2D(scene));
    protos->add("COVER", new ProtoCOVER(scene));
    protos->add("CubeTexture", new ProtoCubeTexture(scene));
    protos->add("Cylinder", new ProtoCylinder(scene));
    protos->add("CylinderSensor", new ProtoCylinderSensor(scene));
    protos->add("DirectionalLight", new ProtoDirectionalLight(scene));
    protos->add("DISEntityManager", new ProtoDISEntityManager(scene));
    protos->add("DISEntityTypeMapping", new ProtoDISEntityTypeMapping(scene));
    protos->add("Disk2D", new ProtoDisk2D(scene));
    protos->add("DoubleAxisHingeJoint", new ProtoDoubleAxisHingeJoint(scene));
    protos->add("EaseInEaseOut", new ProtoEaseInEaseOut(scene));
    protos->add("ElevationGrid", new ProtoElevationGrid(scene));
    protos->add("EspduTransform", new ProtoEspduTransform(scene));
    protos->add("ExplosionEmitter", new ProtoExplosionEmitter(scene));
    protos->add("Extrusion", new ProtoExtrusion(scene));
    protos->add("FillProperties", new ProtoFillProperties(scene));
    protos->add("FloatVertexAttribute", new ProtoFloatVertexAttribute(scene));
    protos->add("Fog", new ProtoFog(scene));
    protos->add("FogCoordinate", new ProtoFogCoordinate(scene));
    protos->add("FontStyle", new ProtoFontStyle(scene));
    protos->add("ForcePhysicsModel", new ProtoForcePhysicsModel(scene));
    protos->add("GeneratedCubeMapTexture", 
                new ProtoGeneratedCubeMapTexture(scene));
    protos->add("GeneratedShadowMap", new ProtoGeneratedShadowMap(scene));
    protos->add("GeoCoordinate", new ProtoGeoCoordinate(scene));
    protos->add("GeoElevationGrid", new ProtoGeoElevationGrid(scene));
    protos->add("GeoLocation", new ProtoGeoLocation(scene));
    protos->add("GeoLOD", new ProtoGeoLOD(scene));
    protos->add("GeoMetadata", new ProtoGeoMetadata(scene));
    protos->add("GeoOrigin", new ProtoGeoOrigin(scene));
    protos->add("GeoPositionInterpolator", 
                new ProtoGeoPositionInterpolator(scene));
    protos->add("GeoProximitySensor", new ProtoGeoProximitySensor(scene));
    protos->add("GeoTouchSensor", new ProtoGeoTouchSensor(scene));
    protos->add("GeoTransform", new ProtoGeoTransform(scene));
    protos->add("GeoViewpoint", new ProtoGeoViewpoint(scene));
    protos->add("GravityPhysicsModel", new ProtoGravityPhysicsModel(scene));
    protos->add("Group", new ProtoGroup(scene));
    protos->add("HAnimDisplacer", new ProtoHAnimDisplacer(scene));
    protos->add("HAnimHumanoid", new ProtoHAnimHumanoid(scene));
    protos->add("HAnimJoint", new ProtoHAnimJoint(scene));
    protos->add("HAnimSegment", new ProtoHAnimSegment(scene));
    protos->add("HAnimSite", new ProtoHAnimSite(scene));
    protos->add("ImageCubeMapTexture", new ProtoImageCubeMapTexture(scene));
    protos->add("ImageTexture", new ProtoImageTexture(scene));
    protos->add("ImageTexture3D", new ProtoImageTexture3D(scene));
    protos->add("IndexedFaceSet", new ProtoIndexedFaceSet(scene));
    protos->add("IndexedLineSet", new ProtoIndexedLineSet(scene));
    protos->add("IndexedQuadSet", new ProtoIndexedQuadSet(scene));
    protos->add("IndexedTriangleFanSet", 
                new ProtoIndexedTriangleFanSet(scene));
    protos->add("IndexedTriangleSet", new ProtoIndexedTriangleSet(scene));
    protos->add("IndexedTriangleStripSet", 
                new ProtoIndexedTriangleStripSet(scene));
    protos->add("Inline", new ProtoInline(scene));
    protos->add("InlineLoadControl", new ProtoInlineLoadControl(scene));
    protos->add("IntegerSequencer", new ProtoIntegerSequencer(scene));
    protos->add("IntegerTrigger", new ProtoIntegerTrigger(scene));
    protos->add("JoystickSensor", new ProtoJoystickSensor(scene));
    protos->add("KambiAppearance", new ProtoKambiAppearance(scene));
    protos->add("KambiHeadLight", new ProtoKambiHeadLight(scene));
    protos->add("KambiInline", new ProtoKambiInline(scene));
    protos->add("KambiNavigationInfo", new ProtoKambiNavigationInfo(scene));
    protos->add("KambiOctreeProperties", new ProtoKambiOctreeProperties(scene));
    protos->add("KambiTriangulation", new ProtoKambiTriangulation(scene));
    protos->add("KeySensor", new ProtoKeySensor(scene));
    protos->add("LabView", new ProtoLabView(scene));
    protos->add("Layer", new ProtoLayer(scene));
    protos->add("LayerSet", new ProtoLayerSet(scene));
    protos->add("Layout", new ProtoLayout(scene));
    protos->add("LayoutGroup", new ProtoLayoutGroup(scene));
    protos->add("LayoutLayer", new ProtoLayoutLayer(scene));
    protos->add("LdrawDatExport", new ProtoLdrawDatExport(scene));
    protos->add("LinePickSensor", new ProtoLinePickSensor(scene));
    protos->add("LineProperties", new ProtoLineProperties(scene));
    protos->add("LineSet", new ProtoLineSet(scene));
    protos->add("LoadSensor", new ProtoLoadSensor(scene));
    protos->add("LocalFog", new ProtoLocalFog(scene));
    protos->add("LOD", new ProtoLOD(scene));
    protos->add("Material", new ProtoMaterial(scene));
    protos->add("Matrix3VertexAttribute",
                new ProtoMatrix3VertexAttribute(scene));
    protos->add("Matrix4VertexAttribute",
                new ProtoMatrix4VertexAttribute(scene));
    protos->add("MatrixTransform", new ProtoMatrixTransform(scene));
    protos->add("MetadataDouble", new ProtoMetadataDouble(scene));
    protos->add("MetadataFloat", new ProtoMetadataFloat(scene));
    protos->add("MetadataInteger", new ProtoMetadataInteger(scene));
    protos->add("MetadataSet", new ProtoMetadataSet(scene));
    protos->add("MetadataString", new ProtoMetadataString(scene));
    protos->add("MotorJoint", new ProtoMotorJoint(scene));
    protos->add("MovieTexture", new ProtoMovieTexture(scene));
    protos->add("MultiGeneratedTextureCoordinate", 
                new ProtoMultiGeneratedTextureCoordinate(scene));
    protos->add("MultiTexture", new ProtoMultiTexture(scene));
    protos->add("MultiTextureCoordinate", 
                new ProtoMultiTextureCoordinate(scene));
    protos->add("MultiTextureTransform", 
                new ProtoMultiTextureTransform(scene));
    protos->add("NavigationInfo", new ProtoNavigationInfo(scene));
    protos->add("Normal", new ProtoNormal(scene));
    protos->add("NormalInterpolator", new ProtoNormalInterpolator(scene));
    protos->add("NurbsCurve", new ProtoNurbsCurve(scene));
    protos->add("NurbsCurve2D", new ProtoNurbsCurve2D(scene));
    protos->add("NurbsGroup", new ProtoNurbsGroup(scene));
    protos->add("NurbsOrientationInterpolator", 
                new ProtoNurbsOrientationInterpolator(scene));
    protos->add("NurbsPositionInterpolator", 
                 new ProtoNurbsPositionInterpolator(scene));
    protos->add("NurbsSet", new ProtoNurbsSet(scene));
    protos->add("NurbsSurface", new ProtoNurbsSurface(scene));
    protos->add("NurbsSurfaceInterpolator", 
                new ProtoNurbsSurfaceInterpolator(scene));
    protos->add("NurbsSweptSurface", new ProtoNurbsSweptSurface(scene));
    protos->add("NurbsSwungSurface", new ProtoNurbsSwungSurface(scene));
    protos->add("NurbsTextureSurface", new ProtoNurbsTextureSurface(scene));
    protos->add("NurbsTextureCoordinate", 
                new ProtoNurbsTextureCoordinate(scene));
    protos->add("NurbsTrimmedSurface", new ProtoNurbsTrimmedSurface(scene));
    protos->add("OdeMotorJoint", new ProtoOdeMotorJoint(scene));
    protos->add("OdeSingleAxisHingeJoint", 
                new ProtoOdeSingleAxisHingeJoint(scene));
    protos->add("OdeSliderJoint", new ProtoOdeSliderJoint(scene));
    protos->add("OrientationChaser", new ProtoOrientationChaser(scene));
    protos->add("OrientationDamper", new ProtoOrientationDamper(scene));
    protos->add("OrientationInterpolator", 
                new ProtoOrientationInterpolator(scene));
    protos->add("OrthoViewpoint", new ProtoOrthoViewpoint(scene));
    protos->add("PackagedShader", new ProtoPackagedShader(scene));
    protos->add("ParticleSystem", new ProtoParticleSystem(scene));
    protos->add("PickableGroup", new ProtoPickableGroup(scene));
    protos->add("PixelTexture", new ProtoPixelTexture(scene));
    protos->add("PixelTexture3D", new ProtoPixelTexture3D(scene));
    protos->add("PlaneSensor", new ProtoPlaneSensor(scene));
    protos->add("PointEmitter", new ProtoPointEmitter(scene));
    protos->add("PointLight", new ProtoPointLight(scene));
    protos->add("PointPickSensor", new ProtoPointPickSensor(scene));
    protos->add("PointSet", new ProtoPointSet(scene));
    protos->add("Polyline2D", new ProtoPolyline2D(scene));
    protos->add("PolylineEmitter", new ProtoPolylineEmitter(scene));
    protos->add("Polypoint2D", new ProtoPolypoint2D(scene));
    protos->add("PositionChaser", new ProtoPositionChaser(scene));
    protos->add("PositionChaser2D", new ProtoPositionChaser2D(scene));
    protos->add("PositionDamper", new ProtoPositionDamper(scene));
    protos->add("PositionDamper2D", new ProtoPositionDamper2D(scene));
    protos->add("PositionInterpolator", new ProtoPositionInterpolator(scene));
    protos->add("PositionInterpolator2D", 
                new ProtoPositionInterpolator2D(scene));
    protos->add("PrimitivePickSensor", new ProtoPrimitivePickSensor(scene));
    protos->add("ProgramShader", new ProtoProgramShader(scene));
    protos->add("ProjectedTextureCoordinate", 
                new ProtoProjectedTextureCoordinate(scene));
    protos->add("ProximitySensor", new ProtoProximitySensor(scene));
    protos->add("QuadSet", new ProtoQuadSet(scene));
    protos->add("ReceiverPdu", new ProtoReceiverPdu(scene));
    protos->add("Rectangle2D", new ProtoRectangle2D(scene));
    protos->add("RenderedTexture", new ProtoRenderedTexture(scene));
    protos->add("RigidBody", new ProtoRigidBody(scene));
    protos->add("RigidBodyCollection", new ProtoRigidBodyCollection(scene));
    protos->add("ScalarChaser", new ProtoScalarChaser(scene));
    protos->add("ScalarInterpolator", new ProtoScalarInterpolator(scene));
    protos->add("ScreenFontStyle", new ProtoScreenFontStyle(scene));
    protos->add("ScreenGroup", new ProtoScreenGroup(scene));
    protos->add("ShaderPart", new ProtoShaderPart(scene));
    protos->add("ShaderProgram", new ProtoShaderProgram(scene));
    protos->add("Shape", new ProtoShape(scene));
    protos->add("SignalPdu", new ProtoSignalPdu(scene));
    protos->add("SingleAxisHingeJoint", new ProtoSingleAxisHingeJoint(scene));
    protos->add("Sky", new ProtoSky(scene));
    protos->add("SliderJoint", new ProtoSliderJoint(scene));
    protos->add("Sound", new ProtoSound(scene));
    protos->add("SpaceSensor", new ProtoSpaceSensor(scene));
    protos->add("Sphere", new ProtoSphere(scene));
    protos->add("SphereSensor", new ProtoSphereSensor(scene));
    protos->add("SplinePositionInterpolator", 
                new ProtoSplinePositionInterpolator(scene));
    protos->add("SplinePositionInterpolator2D", 
                new ProtoSplinePositionInterpolator2D(scene));
    protos->add("SplineScalarInterpolator", 
                new ProtoSplineScalarInterpolator(scene));
    protos->add("SpotLight", new ProtoSpotLight(scene));
    protos->add("SquadOrientationInterpolator", 
                new ProtoSquadOrientationInterpolator(scene));
    protos->add("StaticGroup", new ProtoStaticGroup(scene));
    protos->add("SteeringWheel", new ProtoSteeringWheel(scene));
    protos->add("StringSensor", new ProtoStringSensor(scene));
    protos->add("SuperEllipsoid", new ProtoSuperEllipsoid(scene));
    protos->add("SuperExtrusion", new ProtoSuperExtrusion(scene));
    protos->add("SuperShape", new ProtoSuperShape(scene));
    protos->add("SuperRevolver", new ProtoSuperRevolver(scene));
    protos->add("SurfaceEmitter", new ProtoSurfaceEmitter(scene));
    protos->add("Switch", new ProtoSwitch(scene));
    protos->add("Teapot", new ProtoTeapot(scene));
    protos->add("TexCoordDamper", new ProtoTexCoordDamper(scene));
    protos->add("TexCoordDamper2D", new ProtoTexCoordDamper2D(scene));
    protos->add("Text", new ProtoText(scene));
    protos->add("Text3D", new ProtoText3D(scene));
    protos->add("TextureBackground", new ProtoTextureBackground(scene));
    protos->add("TextureCoordinate", new ProtoTextureCoordinate(scene));
    protos->add("TextureCoordinate3D", new ProtoTextureCoordinate3D(scene));
    protos->add("TextureCoordinate4D", new ProtoTextureCoordinate4D(scene));
    protos->add("TextureCoordinateGenerator", 
                new ProtoTextureCoordinateGenerator(scene));
    protos->add("TextureProperties", new ProtoTextureProperties(scene));
    protos->add("TextureTransform", new ProtoTextureTransform(scene));
    protos->add("TextureTransform3D", new ProtoTextureTransform3D(scene));
    protos->add("TextureTransformMatrix3D", new ProtoTextureTransformMatrix3D(scene));
    protos->add("TimeSensor", new ProtoTimeSensor(scene));
    protos->add("TimeTrigger", new ProtoTimeTrigger(scene));
    protos->add("TouchSensor", new ProtoTouchSensor(scene));
    protos->add("Transform", new ProtoTransform(scene));
    protos->add("TransformSensor", new ProtoTransformSensor(scene));
    protos->add("TransmitterPdu", new ProtoTransmitterPdu(scene));
    protos->add("TriangleFanSet", new ProtoTriangleFanSet(scene));
    protos->add("TriangleSet", new ProtoTriangleSet(scene));
    protos->add("TriangleSet2D", new ProtoTriangleSet2D(scene));
    protos->add("TriangleStripSet", new ProtoTriangleStripSet(scene));
    protos->add("TrimmedSurface", new ProtoTrimmedSurface(scene));
    protos->add("TUIButton", new ProtoTUIButton(scene));
    protos->add("TUIComboBox", new ProtoTUIComboBox(scene));
    protos->add("TUIFloatSlider", new ProtoTUIFloatSlider(scene));
    protos->add("TUIFrame", new ProtoTUIFrame(scene));
    protos->add("TUILabel", new ProtoTUILabel(scene));
    protos->add("TUIListBox", new ProtoTUIListBox(scene));
    protos->add("TUIMap", new ProtoTUIMap(scene));
    protos->add("TUIProgressBar", new ProtoTUIProgressBar(scene));
    protos->add("TUISlider", new ProtoTUISlider(scene));
    protos->add("TUISplitter", new ProtoTUISplitter(scene));
    protos->add("TUITab", new ProtoTUITab(scene));
    protos->add("TUITabFolder", new ProtoTUITabFolder(scene));
    protos->add("TUIToggleButton", new ProtoTUIToggleButton(scene));
    protos->add("TwoSidedMaterial", new ProtoTwoSidedMaterial(scene));
    protos->add("UniversalJoint", new ProtoUniversalJoint(scene));
    protos->add("Vehicle", new ProtoVehicle(scene));
    protos->add("Viewpoint", new ProtoViewpoint(scene));
    protos->add("ViewpointGroup", new ProtoViewpointGroup(scene));
    protos->add("Viewport", new ProtoViewport(scene));
    protos->add("VirtualAcoustics", new ProtoVirtualAcoustics(scene));
    protos->add("VirtualSoundSource", new ProtoVirtualSoundSource(scene));
    protos->add("VisibilitySensor", new ProtoVisibilitySensor(scene));
    protos->add("VolumeEmitter", new ProtoVolumeEmitter(scene));
    protos->add("VolumePickSensor", new ProtoVolumePickSensor(scene));
    protos->add("VrmlCut", new ProtoVrmlCut(scene));
    protos->add("VrmlScene", new ProtoVrmlScene(scene));
    protos->add("Wave", new ProtoWave(scene));
    protos->add("WindPhysicsModel", new ProtoWindPhysicsModel(scene));
    protos->add("WorldInfo", new ProtoWorldInfo(scene));
    protos->add("#", new ProtoComment(scene));
    protos->add("IMPORT", new ProtoImport(scene));
    protos->add("EXPORT", new ProtoExport(scene));

    ProtoMap::Chain::Iterator *j;
    for (int i = 0; i < protos->width(); i++)
        for (j = protos->chain(i).first(); j != NULL; j = j->next())
            if (j->item()->getData()->getType() == -1) {
                swDebugf("internal error: proto of node \"");
                swDebugf(j->item()->getKey());
                swDebugf("\" do not have getType() function");
            }
}
