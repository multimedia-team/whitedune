/*
 * NodeNurbsSwungSurface.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include <stdio.h>

#include "stdafx.h"
#include "NodeNurbsSwungSurface.h"
#include "Scene.h"
#include "FieldValue.h"
#include "SFInt32.h"
#include "Mesh.h"
#include "MFFloat.h"
#include "MFInt32.h"
#include "MFVec2f.h"
#include "MFVec3f.h"
#include "SFNode.h"
#include "SFBool.h"
#include "SFVec3f.h"
#include "Vec2f.h"
#include "Vec3f.h"
#include "RenderState.h"
#include "DuneApp.h"
#include "NodeCoordinate.h"
#include "NodeNormal.h"
#include "NodeTextureCoordinate.h"
#include "NodeIndexedFaceSet.h"
#include "NodeNurbsGroup.h"
#include "NurbsSurfaceDegreeElevate.h"
#include "Util.h"
#include "Field.h"
#include "ExposedField.h"
#include "MoveCommand.h"

ProtoNurbsSwungSurface::ProtoNurbsSwungSurface(Scene *scene)
  : GeometryProto(scene, "NurbsSwungSurface")
{
    profileCurve.set(
          addExposedField(SFNODE, "profileCurve", new SFNode(NULL), 
                          VRML_NURBS_CURVE_2D));
    trajectoryCurve.set(
          addExposedField(SFNODE, "trajectoryCurve", new SFNode(NULL), 
                          VRML_NURBS_CURVE_2D));
    ccw.set(
          addField(SFBOOL, "ccw", new SFBool(true)));
    solid.set(
          addField(SFBOOL, "solid", new SFBool(true)));
}

Node *
ProtoNurbsSwungSurface::create(Scene *scene)
{
    return new NodeNurbsSwungSurface(scene, this); 
}

NodeNurbsSwungSurface::NodeNurbsSwungSurface(Scene *scene, Proto *proto)
  : GeometryNode(scene, proto)
{
}

NodeNurbsSwungSurface::~NodeNurbsSwungSurface()
{
}

void
NodeNurbsSwungSurface::flip(int index)
{
    Node *nprofileCurve  = profileCurve ()->getValue();    
    if (nprofileCurve)
        nprofileCurve->flip(index);
    Node *ntrajectoryCurve = trajectoryCurve()->getValue();    
    if (ntrajectoryCurve)
        ntrajectoryCurve->flip(index);
    SFBool *bccw = new SFBool(!(ccw()->getValue()));
    ccw(bccw);
//    _meshDirty = true;    
}

void
NodeNurbsSwungSurface::swap(int fromTo)
{
    Node *nprofileCurve  = profileCurve ()->getValue();    
    if (nprofileCurve)
        nprofileCurve->swap(fromTo);
    Node *ntrajectoryCurve = trajectoryCurve()->getValue();    
    if (ntrajectoryCurve)
        ntrajectoryCurve->swap(fromTo);
    SFBool *bccw = new SFBool(!(ccw()->getValue()));
    ccw(bccw);
//    _meshDirty = true;    
}

