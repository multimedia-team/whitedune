/*
 * FieldIndex.h
 *
 * Copyright (C) 2005 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _FIELD_INDEX_H
#define _FIELD_INDEX_H

class FieldIndex {
// in principle, this is a readonly integer
// it is used to contain the index of field in a VRML node
// e.g. field "center" of the VRML Transform node is 0, field children is 1 etc.
public:
    FieldIndex()         { _written = 0; }
    FieldIndex(int i)    
                         { 
                         _written = 0;
                         set(i); 
                         }
    void set(int i)      { 
                         if (_written == 0) {
                             _written = 1;
                             if (i < 0) {
                                 assert(0);
                             } else 
                                 _fieldIndex = i;
                         } else 
                             assert(0);
                         }
    operator int() const { 
                         if (_written != 1) assert(0); 
                             return _fieldIndex; 
                         }
protected:
    int _written;
    int _fieldIndex;
};

#endif
