/*
 * Face.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _FACE_H
#define _FACE_H

#ifndef _VEC3F_H
#include "Vec3f.h"
#endif

class Face {
public:
                Face(int numVertices, int offset);

    int         getNumVertices() const          { return _numVertices; }
    int         getOffset() const               { return _offset; }
    const Vec3f&getNormal() const               { return _normal; }
    void        setNormal(const Vec3f &normal)  { _normal = normal; }
    float       getMinZ() const                 { return _minZ; }
    float       getMaxZ() const                 { return _maxZ; }
    void        setMinZ(float minZ)             { _minZ = minZ; }
    void        setMaxZ(float maxZ)             { _maxZ = maxZ; }


private:
    int         _numVertices;
    int         _offset;
    Vec3f       _normal;
    float       _minZ, _maxZ;
};

#endif // _FACE_H
