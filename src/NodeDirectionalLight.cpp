/*
 * NodeDirectionalLight.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeDirectionalLight.h"
#include "Scene.h"
#include "Proto.h"
#include "FieldValue.h"
#include "SFFloat.h"
#include "SFVec3f.h"
#include "SFBool.h"
#include "SFColor.h"
#include "Node.h"
#include "ExposedField.h"
#include "resource.h"


ProtoDirectionalLight::ProtoDirectionalLight(Scene *scene)
  : Proto(scene, "DirectionalLight")
{
    ambientIntensity.set(
          addExposedField(SFFLOAT, "ambientIntensity", new SFFloat(0.0f),
                          new SFFloat(0.0f), new SFFloat(1.0f)));

    color.set(
          addExposedField(SFCOLOR, "color", new SFColor(1.0f, 1.0f, 1.0f)));

    direction.set(    
          addExposedField(SFVEC3F, "direction", 
                          new SFVec3f(0.0f, 0.0f, -1.0f)));

    global.set(
          addExposedField(SFBOOL, "global", new SFBool(false)));
    setFieldFlags(global, FF_X3D_ONLY);

    intensity.set(
          addExposedField(SFFLOAT, "intensity", new SFFloat(1.0f), 
                          new SFFloat(0.0f), new SFFloat(1.0f)));

    on.set(
          addExposedField(SFBOOL, "on", new SFBool(true)));
    kambiLightCommonFields()
    projectionRectangle.set(
        addExposedField(SFVEC4F, "projectionRectangle", new SFVec4f(-10, 10, 
                                                                    -10, 10)));
    setFieldFlags(projectionRectangle, FF_KAMBI_ONLY);
    projectionLocation.set(
        addExposedField(SFVEC3F, "projectionLocation", new SFVec3f(0, 0, 0)));
    setFieldFlags(projectionLocation, FF_KAMBI_ONLY);
}

Node *
ProtoDirectionalLight::create(Scene *scene)
{ 
    return new NodeDirectionalLight(scene, this); 
}

NodeDirectionalLight::NodeDirectionalLight(Scene *scene, Proto *def)
  : Node(scene, def)
{
}


void
NodeDirectionalLight::bind()
{
    const float *fcolor = color()->getValue();
    const float *fdirection = direction()->getValue();

    float ambientColor[4], diffuseColor[4];
    float pos[4];
    for (int i = 0; i < 3; i++) {
        ambientColor[i] = fcolor[i] * ambientIntensity()->getValue();
        diffuseColor[i] = fcolor[i] * intensity()->getValue();
        pos[i] = -fdirection[i];
    }
    ambientColor[3] = diffuseColor[3] = 1.0f;
    pos[3] = 0.0f;

    if (on()->getValue()) {
        _light = (GLenum) _scene->allocateLight();

        glLightfv(_light, GL_AMBIENT, ambientColor);
        glLightfv(_light, GL_DIFFUSE, diffuseColor);
        glLightfv(_light, GL_POSITION, pos);
        glLightfv(_light, GL_SPECULAR, diffuseColor);
        glLightf(_light, GL_SPOT_CUTOFF, 180.0f);
        glLightf(_light, GL_SPOT_EXPONENT, 0.0f);
        glLightf(_light, GL_CONSTANT_ATTENUATION, 1.0f);
        glLightf(_light, GL_LINEAR_ATTENUATION, 0.0f);
        glLightf(_light, GL_QUADRATIC_ATTENUATION, 0.0f);
        glEnable(_light);
    }
}

void NodeDirectionalLight::unbind()
{
    if (on()->getValue()) {
        glDisable(_light);
        _scene->freeLight();
    }
}

int
NodeDirectionalLight::getAnmationCommentID(void) 
{  
    return IDS_ANIMATION_HELP_DIRECTIONALLIGHT + swGetLang(); 
}

const char* 
NodeDirectionalLight::getComponentName(void) const
{
    static const char* name = "Lighting";
    return name;
}

int         
NodeDirectionalLight::getComponentLevel(void) const
{
    if (getParent() != _scene->getRoot())
        return 3;
    return -1;
}

