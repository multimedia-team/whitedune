/*
 * EcmaScriptApp.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "swt.h"
#include "PreferencesApp.h"
#include "EcmaScriptApp.h"
#include "DuneApp.h"

EcmaScriptApp::EcmaScriptApp()
{
    _ecmaScriptAddAvailableFunctions = true;
    _ecmaScriptAddAllowedValues      = true;
    _ecmaScriptAddAllowedComponents  = true;
    _ecmaScriptAddExampleUsage       = true;
    _ecmaScriptAddMathObject         = true;
    _ecmaScriptAddBrowserObject      = true;
}

void EcmaScriptApp::EcmaScriptLoadPreferences()
{
    assert(TheApp != NULL);
    _ecmaScriptAddAvailableFunctions = TheApp->GetBoolPreference(
                                       "EcmaScriptAddAvailableFunctions", true);
    _ecmaScriptAddAllowedValues      = TheApp->GetBoolPreference(
                                       "EcmaScriptAddAllowedValues", true);
    _ecmaScriptAddAllowedComponents  = TheApp->GetBoolPreference(
                                       "EcmaScriptAddAllowedComponents", true);
    _ecmaScriptAddExampleUsage       = TheApp->GetBoolPreference(
                                       "EcmaScriptAddExampleUsage", true);
    _ecmaScriptAddMathObject         = TheApp->GetBoolPreference(
                                       "EcmaScriptAddMathObject", true);
    _ecmaScriptAddBrowserObject      = TheApp->GetBoolPreference(
                                       "EcmaScriptAddBrowserObject", true);
}

void EcmaScriptApp::EcmaScriptSavePreferences(void)
{
    TheApp->SetBoolPreference("EcmaScriptAddAvailableFunctions",
                              _ecmaScriptAddAvailableFunctions);
    TheApp->SetBoolPreference("EcmaScriptAddAllowedValues",
                              _ecmaScriptAddAllowedValues);
    TheApp->SetBoolPreference("EcmaScriptAddAllowedComponents",
                              _ecmaScriptAddAllowedComponents);
    TheApp->SetBoolPreference("EcmaScriptAddExampleUsage",
                              _ecmaScriptAddExampleUsage);
    TheApp->SetBoolPreference("EcmaScriptAddMathObject",
                              _ecmaScriptAddMathObject);
    TheApp->SetBoolPreference("EcmaScriptAddBrowserObject", 
                              _ecmaScriptAddBrowserObject);
}

