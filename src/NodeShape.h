/*
 * NodeShape.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_SHAPE_H
#define _NODE_SHAPE_H

#ifndef _NODE_H
#include "Node.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif

#include "SFMFTypes.h"

class ProtoShape : public Proto {
public:
                    ProtoShape(Scene *scene);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return VRML_SHAPE; }
    virtual int     getNodeClass() const { return SHAPE_NODE | CHILD_NODE; }

    FieldIndex appearance;
    FieldIndex geometry;
    FieldIndex effect;
    FieldIndex bboxCenter;
    FieldIndex bboxSize;
    FieldIndex octreeTriangles;
};

class NodeShape : public Node {
public:
                    NodeShape(Scene *scene, Proto *proto);

    virtual int     getProfile(void) const { return PROFILE_INTERCHANGE; }
    virtual Node   *copy() const { return new NodeShape(*this); }

    virtual void    draw(int pass);
    virtual bool    isLit() const;

    virtual int     countPolygons(void);
    virtual int     countPrimitives(void);

    virtual int     countPolygons1Sided(void);
    virtual int     countPolygons2Sided(void);

    virtual void    flip(int index);
    virtual void    swap(int fromTo);

    virtual float   getTransparency(void);

    virtual bool    hasCoverFields(void) { return true; }   

    virtual int     write(int filedes, int indent);

    virtual Node   *convert2Vrml(void);

    virtual bool    canWriteAc3d();
    virtual bool    canWriteCattGeo();

    void            createNewAppearance(bool emissiveDefaultColor);

    fieldMacros(SFNode,  appearance,      ProtoShape)
    fieldMacros(SFNode,  geometry,        ProtoShape)
    fieldMacros(SFNode,  effect,          ProtoShape)
    fieldMacros(SFVec3f, bboxCenter,      ProtoShape)
    fieldMacros(SFVec3f, bboxSize,        ProtoShape)
    fieldMacros(SFNode,  octreeTriangles, ProtoShape);

protected:
    void            doSpecularPass(Node *appearance, Node *geometry);

protected:
    FieldIndex      _appearance;
    FieldIndex      _geometry;
};

#endif // _NODE_SHAPE_H
