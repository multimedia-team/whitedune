/*
 * NodeBox.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_BOX_H
#define _NODE_BOX_H

#ifndef _MESH_BASED_NODE_H
#include "MeshBasedNode.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif
#ifndef _VEC3F_H
#include "Vec3f.h"
#endif

#include "SFMFTypes.h"

class ProtoBox : public Proto {
public:
                    ProtoBox(Scene *scene);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return VRML_BOX; }
    virtual int     getNodeClass() const 
                       { return PRIMITIVE_GEOMETRY_OR_MASS_DENSITY_MODEL_NODE |
                                GEOMETRY_NODE; }

    FieldIndex size;
    FieldIndex solid;
    FieldIndex texCoord;
};

class NodeBox : public MeshBasedNode {
public:
                    NodeBox(Scene *scene, Proto *proto);

    virtual int     getProfile(void) const { return PROFILE_INTERCHANGE; }
    virtual Node   *copy() const { return new NodeBox(*this); }
    virtual void    draw() { meshDraw(); }
    virtual void    drawHandles();
    virtual Vec3f   getHandle(int handle, int *constraint, int *field);
    virtual void    setHandle(int handle, const Vec3f &v);
    virtual bool    validHandle(int handle);

    virtual Node   *toNurbs(int nuAreaPoints, int uDegree, 
                            int nvAreaPoints, int vDegree);
    virtual Node   *toNurbs(int nuAreaPoints, int uDegree, 
                            int nvAreaPoints, int vDegree, int nzAreaPoints);

    virtual void    flip(int index) {}
    virtual void    swap(int fromTo);

    virtual int     countPrimitives(void) {return 1;}
    virtual int     countPolygons(void) { return 0; }

    virtual bool    isFlat(void) { return true; }

    fieldMacros(SFVec3f, size,     ProtoBox)
    fieldMacros(SFBool,  solid,    ProtoBox)
    fieldMacros(SFNode,  texCoord, ProtoBox)

protected:
    void            createMesh(bool cleanDoubleVertices = true);
    void            makeKnotvectors(int uOrder, int uDimension, 
                                    int vOrder, int vDimension);
    Node           *makeNurbsSurfaces(int surfaces);
    void            makeNurbsRectangle(float xSize, float ySize, 
                                       float zPosition, int nuAreapoints, 
                                       int uDegree);

protected:
    int             _uKnotSize(){return _uKnots.size();}
    int             _vKnotSize(){return _vKnots.size();}
    int             _uOrder;
    int             _vOrder;
    int             _uDimension;
    int             _vDimension;
    int             _nuAreaPoints;
    int             _nvAreaPoints;
    Array<float>    _uKnots;
    Array<float>    _vKnots;
    Array<Vec3f>    _rectangle;
};


#endif // _NODE_BOX_H
