/*
 * SFBoolItem.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "SFBoolItem.h"

#include "FieldView.h"
#include "SFBool.h"
#include "Path.h"
#include "FieldCommand.h"

SFBoolItem::SFBoolItem(FieldView *view) : FieldViewItem(view)
{
    _button = NULL;
}

SFBoolItem::~SFBoolItem()
{
    if (_button) swDestroyWindow(_button);
}

static void
boolButtonCallback(void *data, int id)
{
    ((SFBoolItem *) data)->OnClicked();
}

void
SFBoolItem::Draw(SDC dc, int x, int y)
{
    char buf[128];

    if (((SFBool *) _value)->getValue())
        strcpy(buf, "    TRUE");
    else
        strcpy(buf, "    FALSE");
    swDrawText(dc, x, y + _view->GetItemHeight() - 3, buf);
}

void
SFBoolItem::CreateControl(const Rect &rect, SWND wnd)
{
    _button = swCreateCheckBox("", rect.left, rect.top,
                               rect.Height()-1, rect.Height()-1, wnd);
    swSetCommandCallback(_button, boolButtonCallback);
    swSetClientData(_button, this);
}

void
SFBoolItem::CreateControl(const Rect &rect)
{
    CreateControl(rect, _view->GetWindow());
}


void
SFBoolItem::MoveControl(int x, int y)
{
    if (_button) swSetPosition(_button, x, y);
}

void
SFBoolItem::UpdateControl()
{
    if (_button) swSetCheck(_button, ((SFBool *) _value)->getValue());
}

void
SFBoolItem::OnClicked()
{
    int check = swGetCheck(_button);
    Node *node = _view->GetScene()->getSelection()->getNode();
    FieldValue *value = new SFBool(check != 0);
    _view->GetScene()->execute(new FieldCommand(node, _index, value));
}
