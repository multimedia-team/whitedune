/*
 * StereoViewApp.h
 *
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _STEREO_VIEW_APP_H
#define _STEREO_VIEW_APP_H

#include "swt/include/stereoview.h"

typedef enum {
    EM_LEFT,
    EM_RIGHT,
    EM_NONE
} EyeMode;

typedef enum {
    CM_3DCURSOR_NONE,
    CM_3DCURSOR_RECORDING,
    CM_3DCURSOR_NOT_RUN,
    CM_3DCURSOR_ALWAYS
} Cursor3dMode;

extern bool parseCommandlineArgumentStereoView(int & i,int argc, char** argv);

class StereoViewApp {
public:
                        StereoViewApp();

    bool                canStereo(int stereoType);

    bool                canQuadBufferStereo(void) 
                           { return _canQuadBufferStereo; }

    void                setUseStereo(bool value) 
                           {
                           if (_canStereo) 
                               _useStereo=value;
                           else
                               _useStereo=false;
                           }
    bool                useStereo(void) 
                           {return _useStereo;}

    void                setWantStereo(bool value) {_wantStereo = value;}
    bool                wantStereo(void) {return _wantStereo;}

    void                setStereoType(int value);

    int                 getStereoType(void)
                           {
                           if (useStereo())
                               return _stereoType;
                           else
                               return NO_STEREO;
                           }

    bool                isAnaglyphStereo(void);

    void                setEyeDist(float value) 
                           {
                           _eyeHalfDist = value / 2.0; 
                           accountEyeAngle();
                           }

    void                setEyeScreenDist(float value) 
                           {
                           _eyeScreenDist = value; 
                           accountEyeAngle();
                           }

    float               getEyeHalfDist(void)  {return _eyeHalfDist;}
    float               getEyeAngle(void) {return _eyeAngle;}
    float               getEyeScreenDist(void) {return _eyeScreenDist;}

    EyeMode             getEyeMode(void)  {return _eyeMode;}
    void                setEyeMode(EyeMode value) {_eyeMode = value;}

    float               getEyePosition(void);

    // good stereoviewing may need to ignore the FieldOfView field 
    // of a viewpoint (FieldOfView of the human eye is about 18 Degree)
    void                setFixFieldOfView(float fov) { _fixFieldOfView = fov; }
    float               getFixFieldOfView(void) { return _fixFieldOfView; }
    bool                hasFixFieldOfView(void)
                           { 
                           if (_fixFieldOfView > 0) 
                               return true;
                           else
                               return false;
                           }
    float               GetStereoHandleSizeMult(void) 
                           { return _stereoHandleSizeMult; }
    void                SetStereoHandleSizeMult(float m) 
                           { _stereoHandleSizeMult = m; }

    Cursor3dMode        Get3dCursorMode(void) {return _cursor3dMode; }
    void                Set3dCursorMode(Cursor3dMode cursor3dMode)
                           {
                           _cursor3dMode = cursor3dMode;
                           }
    float               Get3dCursorWidth(void) { return _cursor3dWidth; }
    void                Set3dCursorWidth(float w) { _cursor3dWidth = w; }
    float               Get3dCursorLength(void) { return _cursor3dLength; }
    void                Set3dCursorLength(float l) { _cursor3dLength = l; }

    float               GetEyeAngleFactor(void) { return _eyeAngleFactor; }
    void                SetEyeAngleFactor(float factor) 
                           { 
                           _eyeAngleFactor = factor; 
                           accountEyeAngle();
                           }

    void                StereoViewSetDefaults();

    void                StereoViewLoadPreferences();
    void                StereoViewSavePreferences();

private:
    void                accountEyeAngle(void);

private:
    bool                _canStereo;
    bool                _canQuadBufferStereo;
    bool                _canAnaglyphStereo;
    bool                _useStereo;
    bool                _wantStereo;
    int                 _stereoType;
    EyeMode             _eyeMode;
    float               _eyeHalfDist;
    float               _eyeAngle;
    float               _eyeAngleFactor;
    float               _eyeScreenDist;
    float               _fixFieldOfView;
    float               _stereoHandleSizeMult;
    Cursor3dMode        _cursor3dMode;
    float               _cursor3dWidth;
    float               _cursor3dLength;
};

#endif

