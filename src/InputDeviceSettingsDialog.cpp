/*
 * InputDeviceSettingsDialog.cpp
 *
 * Copyright (C) 2000 Stephen F. White, 2004 Haining Zhi
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

//////////////////////////////////////////////////////////////////////
// InputDeviceSettingsDialog.cpp: implementation of the 
// InputDeviceSettingsDialog class.
// This Programm collect the InputDevice informations and format them.
// Haining Zhi Mai.2004 ICA Uni-Stuttgart
// A part of project white_dune.
//////////////////////////////////////////////////////////////////////

#include "DuneApp.h"
#include "InputDeviceSettingsDialog.h"
#include "AflockSettingsDialog.h"
#include "stdafx.h"
#include "resource.h"
#include "swt.h"

extern bool parseCommandlineArgumentInputDevice(int & i,int argc, char** argv);

InputDeviceSettingsDialog::InputDeviceSettingsDialog(SWND Parent) :
      Dialog(Parent, IDD_INPUT_DEVICE)
{
    parent = Parent;
    combo_devices =    swGetDialogItem(_dlg, IDC_COMBO_INPUTDEVICES);
    combo[0] =         swGetDialogItem(_dlg, IDC_COMBO_X);
    combo[1] =         swGetDialogItem(_dlg, IDC_COMBO_Y);
    combo[2] =         swGetDialogItem(_dlg, IDC_COMBO_Z);
    combo[3] =         swGetDialogItem(_dlg, IDC_COMBO_RX);
    combo[4] =         swGetDialogItem(_dlg, IDC_COMBO_RY);
    combo[5] =         swGetDialogItem(_dlg, IDC_COMBO_RZ);
    edit_max_axis =    swGetDialogItem(_dlg, IDC_EDIT_MAX_AXIS);
    commandline =      swGetDialogItem(_dlg, IDC_COMMANDLINE);
    edit_device_name = swGetDialogItem(_dlg, IDC_EDIT_DEVICE_NAME);
    check_sendalways = swGetDialogItem(_dlg, IDC_CHECK_SENDALWAYS);
    check_all =        swGetDialogItem(_dlg, IDC_SEL_ALL);
    check_allxyz =     swGetDialogItem(_dlg, IDC_SEL_ALLXYZ);
    check_allrot =     swGetDialogItem(_dlg, IDC_SEL_ALLROT);
    check_dcfocus =    swGetDialogItem(_dlg, IDC_CHECK_DCFOCUS);
    edit_factor[0] =   swGetDialogItem(_dlg, IDC_FACTORX);
    edit_factor[1] =   swGetDialogItem(_dlg, IDC_FACTORY);
    edit_factor[2] =   swGetDialogItem(_dlg, IDC_FACTORZ);
    edit_factor[3] =   swGetDialogItem(_dlg, IDC_FACTORRX);
    edit_factor[4] =   swGetDialogItem(_dlg, IDC_FACTORRY);
    edit_factor[5] =   swGetDialogItem(_dlg, IDC_FACTORRZ);
    edit_accel[0] =    swGetDialogItem(_dlg, IDC_ACCELX);
    edit_accel[1] =    swGetDialogItem(_dlg, IDC_ACCELY);
    edit_accel[2] =    swGetDialogItem(_dlg, IDC_ACCELZ);
    edit_accel[3] =    swGetDialogItem(_dlg, IDC_ACCELRX);
    edit_accel[4] =    swGetDialogItem(_dlg, IDC_ACCELRY);
    edit_accel[5] =    swGetDialogItem(_dlg, IDC_ACCELRZ);
    check_wheel[0] =   swGetDialogItem(_dlg, IDC_WHEELX);
    check_wheel[1] =   swGetDialogItem(_dlg, IDC_WHEELY);
    check_wheel[2] =   swGetDialogItem(_dlg, IDC_WHEELZ);
    check_wheel[3] =   swGetDialogItem(_dlg, IDC_WHEELRX);
    check_wheel[4] =   swGetDialogItem(_dlg, IDC_WHEELRY);
    check_wheel[5] =   swGetDialogItem(_dlg, IDC_WHEELRZ);
    edit_ignore[0] =   swGetDialogItem(_dlg, IDC_IGNOREX);
    edit_ignore[1] =   swGetDialogItem(_dlg, IDC_IGNOREY);
    edit_ignore[2] =   swGetDialogItem(_dlg, IDC_IGNOREZ);
    edit_ignore[3] =   swGetDialogItem(_dlg, IDC_IGNORERX);
    edit_ignore[4] =   swGetDialogItem(_dlg, IDC_IGNORERY);
    edit_ignore[5] =   swGetDialogItem(_dlg, IDC_IGNORERZ);
    check_not[0] =     swGetDialogItem(_dlg, IDC_NX);
    check_not[1] =     swGetDialogItem(_dlg, IDC_NY);
    check_not[2] =     swGetDialogItem(_dlg, IDC_NZ);
    check_not[3] =     swGetDialogItem(_dlg, IDC_NRX);
    check_not[4] =     swGetDialogItem(_dlg, IDC_NRY);
    check_not[5] =     swGetDialogItem(_dlg, IDC_NRZ);

    dummyInputDevice device;

    axes_name[0] = device.getAxesinfo()[3];
    axes_name[1] = device.getAxesinfo()[4];
    axes_name[2] = device.getAxesinfo()[5];
    axes_name[3] = device.getAxesinfo()[0];
    axes_name[4] = device.getAxesinfo()[1];
    axes_name[5] = device.getAxesinfo()[2];
    axes_name[6] = "all";
    axes_name[7] = "allxyz";
    axes_name[8] = "allrot";
    //if char * device_option[10] is changed, 
    //then device_index[10] must also be changed.
    swComboBoxDeleteAll(combo_devices);
    device_options = 0;

    swComboBoxAppendItem(combo_devices,"none");
    device_option[device_options] = "";
    device_index[device_options]=device_options;
    device_options++;

#ifdef LINUX_JOYSTICK
    swComboBoxAppendItem(combo_devices,"-joystick");
    device_option[device_options] = "-joystick";
    device_index[device_options]=device_options;
    device_options++;
#else
# ifdef WINDOWS_JOYSTICK
    swComboBoxAppendItem(combo_devices,"-joystick");
    device_option[device_options] = "-joystick";
    device_index[device_options]=device_options;
    device_options++;
# endif
#endif

#ifdef HAVE_XINPUT
    swComboBoxAppendItem(combo_devices,"-xinput");
    device_option[device_options] = "-xinput";
    device_index[device_options]=device_options;
    device_options++;
#endif

#ifdef HAVE_LIBSBALL
    swComboBoxAppendItem(combo_devices,"-spaceball");
    device_option[device_options] = "-spaceball";
    device_index[device_options]=device_options;
    device_options++;
#endif

#ifdef HAVE_NXT_DIALS
    swComboBoxAppendItem(combo_devices,"-nxtdials");
    device_option[device_options] = "-nxtdials";
    device_index[device_options]=device_options;
    device_options++;
#endif

#ifdef HAVE_WINDOWS_P5
    swComboBoxAppendItem(combo_devices,"-p5");
    device_option[device_options] = "-p5";
    device_index[device_options]=device_options;
    device_options++;
#endif

#ifdef HAVE_AFLOCK
    swComboBoxAppendItem(combo_devices,"-aflock");
    device_option[device_options] = "-aflock";
    device_index[device_options]=device_options;
    device_options++;
#endif

#ifdef HAVE_SDL_JOYSTICK
    swComboBoxAppendItem(combo_devices,"-SDLjoystick");
    device_option[device_options] = "-SDLjoystick";
    device_index[device_options]=device_options;
    device_options++;
#endif

    for (i = 0; (i < TheApp->getNumberInputDevices()) && (i < 256); i++) {
        char buf[256];
        InputDevice *inputDevice=TheApp->getInputDevice(i);
        mysnprintf(buf, 255, "%s %s", inputDevice->getDeviceOption(), 
                                      inputDevice->getDeviceName());
        swComboBoxAppendItem(combo_devices,buf);
    }

    max_axes=6;
    swComboBoxSetSelection(combo_devices, 0);
    enable_all(false);
}
 
void InputDeviceSettingsDialog::OnCommand(int id)
{     
    switch(id) {
#ifdef HAVE_AFLOCK
      case IDC_OPTIONS_AFLOCK:
        {
        if (TheApp->getNumberAflockDevices() > 0) {
            AflockSettingsDialog dlg(parent);
            dlg.DoModal();
        } else {
            swMessageBox(_dlg, "no aflock device yet", "white_dune", SW_MB_OK, 
                         SW_MB_ERROR);
        }
        break;
        }
#endif
      case IDC_COMBO_INPUTDEVICES:
        LoadComboBoxData();
        LoadData();
        break;
      case IDC_SAVE: 
        SaveData();  
        break;
      case IDC_OKSAVE:
        //save and close dialog
        SaveData();
        OnCommand(IDOK);
        break;
      case IDC_BUTTON_DEFAULT:
        //set default values
        SetDefault();
        break;
      case IDC_EDIT_DEVICE_NAME:
        LoadData();
        break;
      //click all,allxyz,allrot check buttons.
      case IDC_SEL_ALL:
        select_all();
        break;
      case IDC_SEL_ALLXYZ:
        select_xyz();
        break;
      case IDC_SEL_ALLROT:
        select_rot();
        break;         
      default:
        Dialog::OnCommand(id);
        break;
    }
}

void InputDeviceSettingsDialog::LoadComboBoxData(void)
{
    int selection=swComboBoxGetSelection(combo_devices);
    const char *current_option="";
    if (selection<=0) {
        swSetText(edit_device_name,"");
        swEnableWindow(swGetDialogItem(_dlg,IDC_EDIT_DEVICE_NAME),false);
        return;
    } else if (selection<device_options) {
        current_option=device_option[selection];
        swGetText(edit_device_name,buf,128); 
        if (strlen(buf)==0) {
            const char *device_name=guessFirstDeviceName(current_option);
#ifdef HAVE_XINPUT
            if (strcmp(current_option,"-xinput")==0)
                device_name=swGuessFirstXinputDevice();
#endif
            swSetText(edit_device_name,device_name);        
        }
    } else {
        int device_nr=selection-device_options-1;
        InputDevice *inputDevice=TheApp->getInputDevice(device_nr);
        swSetText(edit_device_name,inputDevice->getDeviceName());
    }
    swEnableWindow(swGetDialogItem(_dlg,IDC_EDIT_DEVICE_NAME),true);
}

bool InputDeviceSettingsDialog::Validate()
{
    int selection=swComboBoxGetSelection(combo_devices);
    swGetText(edit_device_name,buf,128); 
    if ((selection==0)||(buf[0]==0))
        return false;

    int device_nr=-1;
    if (selection<(device_options))
        device_nr=TheApp->searchInputDevice(device_option[selection],buf);
    else
        device_nr=selection-device_options;
    if (device_nr<0) {     
        InputDevice *device = createInputDevice(device_option[selection],buf);
        TheApp->setInputDevice(device);
        device_nr=TheApp->searchInputDevice(device_option[selection],buf);
        if (device_nr<0)
            return false;
    }

    //read aflock on/off
#ifdef HAVE_AFLOCK
    swEnableWindow(swGetDialogItem(_dlg, IDC_OPTIONS_AFLOCK),true);
#else
    swEnableWindow(swGetDialogItem(_dlg, IDC_OPTIONS_AFLOCK),false);
#endif

    return true;
}    

void InputDeviceSettingsDialog::LoadData() 
{
    if (!Validate())
        return;
    int selection=swComboBoxGetSelection(combo_devices);
    swGetText(edit_device_name,buf,128); 
    int device_nr=-1;
    if (selection<(device_options))
        device_nr=TheApp->searchInputDevice(device_option[selection],buf);
    else
        device_nr=selection-device_options;
    if (device_nr<0)
        return;
    InputDevice *inputDevice=TheApp->getInputDevice(device_nr);

    //clear axis combo_axes
    for (i=0;i<6;i++) {
        swComboBoxDeleteAll(combo[i]);
    }
    //get the max_number of axes.
    max_axis=inputDevice->get_number_max_axis();
    mysnprintf(buf,128,"%d", max_axes);
    swSetText(edit_max_axis,buf);
    max_axes=max_axis;

    //init the combo_axes informationen
    for (i=0;i<max_axis;i++) {
        mysnprintf(buf, 128, "%d", i);
        for (j=0;j<6;j++) {
            swComboBoxAppendItem(combo[j], buf);
        }
    }
    for (i=0;i<6;i++) {
        swComboBoxAppendItem(combo[i], "none");
    }

    //read the combo_axes Selection
    for (j=0;j<6;j++) {
        swComboBoxSetSelection(combo[j],
               inputDevice->getAxisFromInformation(axes_name[j]));
        if (inputDevice->getIgnore(j))
            swComboBoxSetSelection(combo[j],max_axis);
    }

    //read sendalways
    swSetCheck(check_sendalways,inputDevice->sendalways());
    //read (don't care focus)
    swSetCheck(check_dcfocus,TheApp->dontCareFocus());

    //read nagation,factor,accel,wheel,ignore
    for (i=0;i<max_axes;i++) {
        if (inputDevice
            ->get_sign(swComboBoxGetSelection(combo[i]))==-1)
        swSetCheck(check_not[i],1);
        if (inputDevice->get_sign(swComboBoxGetSelection(combo[i]))==1)
            swSetCheck(check_not[i],0);

        mysnprintf(buf, 128, "%g", 
                   inputDevice->get_factor(swComboBoxGetSelection(combo[i])));
        swSetText(edit_factor[i],buf);

        mysnprintf(buf, 128, "%g", 
              inputDevice->get_acceleration(swComboBoxGetSelection(combo[i])));
        swSetText(edit_accel[i],buf);

        swSetCheck(check_wheel[i],!inputDevice->get_zero_on_release(
                                         swComboBoxGetSelection(combo[i])));

        mysnprintf(buf, 128, "%g", inputDevice->get_zero_size_fraction(
              swComboBoxGetSelection(combo[i])));
        swSetText(edit_ignore[i],buf);
    }
    for (i=max_axes;i<6;i++) { 
        swSetCheck(check_not[i],0);
        swSetText(edit_factor[i],"");
        swSetText(edit_accel[i],"");
        swSetCheck(check_wheel[i],false);
        swSetText(edit_ignore[i],"");

    }
}

void InputDeviceSettingsDialog::addAxesData(MyString string, 
                                            SWND edit_factor, SWND edit_accel,
                                            bool wheel, SWND edit_ignore)
{
    bool hasFactor = true;
    char factor[128];
    swGetText(edit_factor,factor,128);
    if (strcmp(factor,"0.0")==0)
        hasFactor = false;
    bool hasAccel = true;
    char accel[128];
    swGetText(edit_accel,accel,128);
    if (strcmp(accel,"1.0")==0)
        hasAccel = false;
    bool hasIgnore = true;
    char ignore[128];
    swGetText(edit_ignore,ignore,128);
    if (strcmp(ignore,"0.0")==0)
    if (hasIgnore == 0.0)
        hasIgnore = false;
    if (hasIgnore || wheel || hasAccel || hasFactor) {
        if (hasFactor) 
            string += factor;
        if (hasIgnore || wheel || hasAccel) {
            string += ",";
            if (hasAccel)
                string += accel;
            if (hasIgnore || wheel) {
                string += ",";
                if (wheel)
                    string += "wheel";
                if (hasIgnore) {
                    string += ",";
                    string += ignore;
                }
            }
        }   
    }
} 

void InputDeviceSettingsDialog::addAxisData(MyString string, int axis, 
                                            SWND factor, SWND accel,
                                            bool wheel, SWND ignore)
{
    char buf[128];
    string += "=";
    mysnprintf(buf,128,"%d",axis);
    string += buf;
    string += ",";
    addAxesData(string, factor, accel, wheel, ignore);
} 

void InputDeviceSettingsDialog::SaveData()
{
    //click button save_Settings 

    int selection=swComboBoxGetSelection(combo_devices);
    if (selection!=0) {
        char device[128];
        swGetText(edit_device_name,device,128); 

        // write all paramater to command[]
        Array<MyString> command;
        command_sum=0;

        //check the max_number_axis

        command[command_sum] = "";

        command[command_sum] += device_option[selection];
        command[command_sum] += " ";
        command[command_sum] += device;

        command[command_sum] += "-axes=";
        swGetText(edit_max_axis,buf,128);
        command[command_sum] += buf;
        command_sum++;

        // set the axis information

        bool axesdata_xyz = false;
        bool axesdata_rot = false;
        if (swGetCheck(check_all)) {
            axesdata_xyz = false;
            axesdata_rot = false;

            command[command_sum] = "";
            command[command_sum] += "-all=";
            addAxesData(command[command_sum], edit_factor[0], edit_accel[0], 
                        swGetCheck(check_wheel[0]), edit_ignore[0]);
            command_sum++;

        } else if (swGetCheck(check_allxyz)&&swGetCheck(check_allrot)) {
            axesdata_xyz = false;
            axesdata_rot = false;

            command[command_sum] = "";
            command[command_sum] += "-allxyz=";
            addAxesData(command[command_sum], edit_factor[0], edit_accel[0], 
                        swGetCheck(check_wheel[0]), edit_ignore[0]);
            command_sum++;

            command[command_sum] = "";
            command[command_sum] += "-allrot=";
            addAxesData(command[command_sum], edit_factor[3], edit_accel[3],
                        swGetCheck(check_wheel[3]), edit_ignore[3]);
            command_sum++;

        } else if (swGetCheck(check_allxyz)) {
            axesdata_xyz = false;

            command[command_sum] = "";
            command[command_sum] += "-allxyz=";
            addAxesData(command[command_sum], edit_factor[0], edit_accel[0], 
                        swGetCheck(check_wheel[0]), edit_ignore[0]);
            command_sum++;

        } else if (swGetCheck(check_allrot)) {
            axesdata_rot = false;

            command[command_sum] = "";
            command[command_sum] += "-allrot=";
            addAxesData(command[command_sum], edit_factor[3], edit_accel[3],
                        swGetCheck(check_wheel[3]), edit_ignore[3]);
            command_sum++;
  
        }
        for (i=0;i<6;i++) {
            command[command_sum] = "";
            if (swComboBoxGetSelection(combo[i])<max_axis) {
                command[command_sum] += "-";
                command[command_sum] += axes_name[i];
                int axis = swComboBoxGetSelection(combo[i]);
                if (swGetCheck(check_not[i]))
                    axis = -axis;
                command[command_sum] += "=";
                if (((axesdata_xyz) && (i > 2)) ||
                    ((axesdata_rot) && (i < 3))) {
                    mysnprintf(buf,128,"%d ",axis);
                    command[command_sum] += buf;
                } else 
                    addAxisData(command[command_sum], axis,
                                edit_factor[i], edit_accel[i], 
                                swGetCheck(check_wheel[i]), edit_ignore[i]);
            } else if (swComboBoxGetSelection(combo[i])>=max_axis) {
                command[command_sum] += "-";
                command[command_sum] += axes_name[i];
                command[command_sum] += "=none ";
            }
            command_sum++;
        }

        //set sendalways
        if (swGetCheck(check_sendalways)) { 
            command[command_sum] = "";
            command[command_sum] += "-sendalways ";
            command_sum++;
        }
    
        //set don't care focus
        if (swGetCheck(check_dcfocus)) { 
            command[command_sum] = "";
            command[command_sum] += "-dontcarefocus ";
            command_sum++;
        }

        //print the commandline

        MyString line = "";

        for (i=0;i<command_sum;i++) {
            line += " ";
            line += command[i];
        }

        swSetText(commandline,line);

        //run the Commandline
        char **command_line = new char*[command_sum];
        for (i=0; i < command_sum; i++)
            command_line[i] = (char *)(const char *)command[i];
        for (i=0; i < command_sum; i++)
           if (!parseCommandlineArgumentInputDevice(i,command_sum,command_line))
                break;

        delete [] command_line;

        //Update the dialog
        LoadData();
        TheApp->PrintMessageWindowsId(IDS_INPUT_DEVICE_PARAMETERS_SAVED);
    }
}

void InputDeviceSettingsDialog::select_xyz()
{
    
    if (swGetCheck(check_allxyz)) {
        swEnableWindow(edit_factor[0],true);
        swEnableWindow(edit_factor[1],false);
        swEnableWindow(edit_factor[2],false);
        swEnableWindow(edit_accel[0],true);
        swEnableWindow(edit_accel[1],false);
        swEnableWindow(edit_accel[2],false);
        swEnableWindow(check_wheel[0],true);
        swEnableWindow(check_wheel[1],false);
        swEnableWindow(check_wheel[2],false);
        swEnableWindow(edit_ignore[0],true);
        swEnableWindow(edit_ignore[1],false);
        swEnableWindow(edit_ignore[2],false);
    } else {
         for (i=0;i<3;i++) {
             swEnableWindow(edit_factor[i],true);
             swEnableWindow(edit_accel[i],true);
             swEnableWindow(check_wheel[i],true);
             swEnableWindow(edit_ignore[i],true);
         }
    }
}

void InputDeviceSettingsDialog::select_rot()
{
    if (swGetCheck(check_allrot)) {
        swEnableWindow(edit_factor[3],true);
        swEnableWindow(edit_factor[4],false);
        swEnableWindow(edit_factor[5],false);
        swEnableWindow(edit_accel[3],true);
        swEnableWindow(edit_accel[4],false);
        swEnableWindow(edit_accel[5],false);
        swEnableWindow(check_wheel[3],true);
        swEnableWindow(check_wheel[4],false);
        swEnableWindow(check_wheel[5],false);
        swEnableWindow(edit_ignore[3],true);
        swEnableWindow(edit_ignore[4],false);
        swEnableWindow(edit_ignore[5],false);
    } else {
        for (i=3;i<6;i++) {
            swEnableWindow(edit_factor[i],true);
            swEnableWindow(edit_accel[i],true);
            swEnableWindow(check_wheel[i],true);
            swEnableWindow(edit_ignore[i],true);
        }
    }
}

void InputDeviceSettingsDialog::select_all()
{
    
    if (swGetCheck(check_all)) {
        swEnableWindow(check_allxyz,false);
        swEnableWindow(check_allrot,false);
        for (i=1;i<6;i++) {
            swEnableWindow(edit_factor[i],false);
            swEnableWindow(edit_accel[i],false);
            swEnableWindow(check_wheel[i],false);
            swEnableWindow(edit_ignore[i],false);
        }                
    } else {
        swEnableWindow(check_allxyz,true);
        swEnableWindow(check_allrot,true);
        select_xyz();
        select_rot();
    }
}


void InputDeviceSettingsDialog::enable_all(bool flag)
{
    
     swEnableWindow(check_allxyz,flag);
     swEnableWindow(check_allrot,flag);
     for (i=0;i<6;i++) {
         swEnableWindow(edit_factor[i],flag);
         swEnableWindow(edit_accel[i],flag);
         swEnableWindow(check_wheel[i],flag);
         swEnableWindow(edit_ignore[i],flag);
         swEnableWindow(check_not[i],flag);
     }                
     swEnableWindow(check_sendalways,flag);
     swEnableWindow(check_all,flag);
     swEnableWindow(check_allxyz,flag);
     swEnableWindow(check_allrot,flag);
     swEnableWindow(edit_max_axis,flag);
     swEnableWindow(commandline,flag);
     swEnableWindow(edit_device_name,flag);
     swEnableWindow(check_dcfocus,flag);
}

void InputDeviceSettingsDialog::SetDefault()
{
    command_sum=0;
    char **command_line = new char*[MAX_LEN_COMMANDLINE];
    command_line[command_sum] = new char[128];
    mysnprintf(command_line[command_sum++],128,"%s","-axes=6");
    command_line[command_sum] = new char[128];
    mysnprintf(command_line[command_sum++],128,"%s","-x=0");
    command_line[command_sum] = new char[128];
    mysnprintf(command_line[command_sum++],128,"%s","-y=1");
    command_line[command_sum] = new char[128];
    mysnprintf(command_line[command_sum++],128,"%s","-z=2");
    command_line[command_sum] = new char[128];
    mysnprintf(command_line[command_sum++],128,"%s","-xrot=3");
    command_line[command_sum] = new char[128];
    mysnprintf(command_line[command_sum++],128,"%s","-yrot=4");
    command_line[command_sum] = new char[128];
    mysnprintf(command_line[command_sum++],128,"%s","-zrot=5");
    command_line[command_sum] = new char[128];
    mysnprintf(command_line[command_sum++],128,"%s","-all=1,1,,0");

    //print the commandline

    MyString line = "";
    for (i=0;i<command_sum;i++) {
        line += " ";
        line += command_line[i];
    }

    swSetText(commandline,line);

    //run the Commandline
    i=0;
    while (i<command_sum) {
        if (!parseCommandlineArgumentInputDevice(i,command_sum,command_line))
            break;
        i++;
    }
    delete command_line;

    //Update the dialog
    LoadData();
}

