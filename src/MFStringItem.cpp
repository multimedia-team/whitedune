/*
 * MFStringItem.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "MFStringItem.h"
#include "MFString.h"
#include "SFStringItem.h"
#include "SFString.h"
#include "FieldView.h"
#include "swt.h"

MFStringItem::MFStringItem(FieldView *view) : MFieldViewItem(view)
{
}

FieldViewItem *
MFStringItem::CreateSFItem()
{
    return new SFStringItem(_view);
}

//
// when an item is collapsed, draw the 1st element
//

void
MFStringItem::Draw(SDC dc, int x, int y)
{
    int size = _view->GetItemHeight() - 1;
    if (IsCollapsed()) {
        if (((MFString *) _value)->getSize()) {
            _children[0]->Draw(dc, x, y);
        } else if (_field->getFlags() & FF_URL) {
            int bx = x + _view->GetItemWidth() - size;
            swDraw3DRect(dc, _view->GetWindow(), bx, y, size, size);
            swSetFGColor(dc, swGetWindowColor(_view->GetWindow(), SW_COLOR_TEXT));
            swDrawText(dc, bx + 2, y + _view->GetItemHeight() - 3, "...");
        }
    }
}

//
// allow the 1st element to be edited, when the item is collapsed
//

bool
MFStringItem::IsEditable() const
{
    return !IsCollapsed() || (((MFString *)_value)->getSize() < 2);
}

void
MFStringItem::StartEditing(MyString &str, int offset)
{
    MFString *v = (MFString *) _value;

    if (!IsCollapsed() || (v->getSize() == 0)) {
        InsertSFValue(0);
        _children.insert(new FieldViewItem(_view), 0); 
    } else
        str = "";
    InitIndexValue(0, _value);
    str = v->getValue(0);
}

FieldValue *
MFStringItem::StopEditing(const char *str, int offset)
{
    ((MFString *)_value)->setSFValue(offset, str);
    InitIndexValue(offset, _value);
    return _value;
}

void
MFStringItem::InsertSFValue(int index)
{
    ((MFString *)_value)->insertSFValue(index, "");
}

void                
MFStringItem::RemoveSFValue(int index) 
{
    ((MFString *)_value)->removeSFValue(index);
}

FieldValue *
MFStringItem::OnMouseDown(int x, int y, int modifiers)
{
    int width = _view->GetItemWidth();
    int buttonSize = _view->GetItemHeight() - 1;

    if (x >= width - buttonSize && x < width) {
        if (_field->getFlags() & FF_URL) {
/*
            CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, 
                            "Image Files (*.gif, *.jpg)|*.gif;*.jpg|All Files (*.*)|*.*||",
                            _view);

            if (dlg.DoModal() == IDOK) {
                URL parent, child;
                const char *url = _view->GetDocument()->getScene()->getURL();

                if (url[0]) {
                    parent = url;
                } else {
                    char cdir[_MAX_DIR];
                    GetCurrentDirectory(_MAX_DIR, cdir);
                    parent.FromDOSPath(cdir);
                }
                child.FromDOSPath(dlg.GetPathName());
                return new MFString(child.RelativeTo(parent));
            }
*/
        } else if (_field->getStrings()) {
            // create popup
        }
    }
    return NULL;
}
