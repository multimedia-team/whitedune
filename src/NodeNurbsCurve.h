/*
 * NodeNurbsCurve.h
 *
 * Copyright (C) 2003 Th. Rothermel
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_NURBS_CURVE_H
#define _NODE_NURBS_CURVE_H

#ifndef _GEOMETRY_NODE_H
# include "GeometryNode.h"
#endif
#ifndef _PROTO_MACROS_H
# include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
# include "Proto.h"
#endif
#ifndef _VEC3F_H
# include "Vec3f.h"
#endif
#ifndef _SFMFTYPES_H
# include "SFMFTypes.h"
#endif
#ifndef _VEC3F
# include "Vec3f.h"
#endif
#include "ChainBasedNode.h"

typedef enum {
    NURBS_ROT_X_AXIS,
    NURBS_ROT_Y_AXIS,
    NURBS_ROT_Z_AXIS,
    NURBS_ROT_POINT_TO_POINT
} NurbsRot;

class MFVec3f;

class ProtoNurbsCurve : public ChainBasedProto {
public:
                    ProtoNurbsCurve(Scene *scene);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return VRML_NURBS_CURVE; }
    virtual int     getNodeClass() const
                       { return PARAMETRIC_GEOMETRY_NODE | GEOMETRY_NODE; }

    FieldIndex controlPoint;
    FieldIndex controlPointX3D;
    FieldIndex tessellation;
    FieldIndex weight;
    FieldIndex closed;
    FieldIndex knot;
    FieldIndex order;
};

class NodeNurbsCurve : public ChainBasedNode {
public:
                    NodeNurbsCurve(Scene *scene, Proto *proto);

    virtual Node   *copy() const { return new NodeNurbsCurve(*this); }

    virtual const char* getComponentName(void) const { return "NURBS"; }
    virtual int     getComponentLevel(void) const { return 1; }

    virtual void    createChain(void);

    virtual void    drawHandles();

    virtual Vec3f   getHandle(int handle, int *constraint, int *field);
    virtual void    setHandle(int handle, const Vec3f &v);

    virtual void    update();

    virtual bool    avoidProtoOnPureVrml(void) { return true; }
    int             writeProto(int filedes);
    int             write(int filedes, int indent);

    bool            revolveFlatten(int direction);
    void            revolveFlatter(int zero, int change);

    Node           *toSuperExtrusion(void);
    Node           *toSuperRevolver(void);
    virtual Node   *toNurbs(int narcs, int pDegree, float rDegree, Vec3f &P1, Vec3f &P2);

    virtual Node   *degreeElevate(int newDegree); 
    virtual bool    hasBoundingBox(void) { return true; }
    virtual void    flip(int index);
    virtual void    swap(int fromTo);
    virtual void    flatten(int direction, bool zero);              
    virtual bool    canFlatten(void) {return true;}

    virtual bool    maySetDefault(void) { return false; }

    Node           *convert2X3d(void);
    Node           *convert2Vrml(void);

//protected:
    fieldMacros(MFVec3f, controlPoint,    ProtoNurbsCurve)
    fieldMacros(SFNode,  controlPointX3D, ProtoNurbsCurve)
//public:
    fieldMacros(SFInt32, tessellation,    ProtoNurbsCurve)
    fieldMacros(MFFloat, weight,          ProtoNurbsCurve)
    fieldMacros(SFBool,  closed,          ProtoNurbsCurve)
    fieldMacros(MFFloat, knot,            ProtoNurbsCurve)
    fieldMacros(SFInt32, order,           ProtoNurbsCurve)

    virtual void    setInternal(bool flag) { _isInternal = flag; }
    MFVec3f        *getControlPoints(void);
    void            setControlPoints(const MFVec3f *points);
    void            createControlPoints(const MFVec3f *points);
    void            backupFieldsAppend(int field);

protected:
    static int      findSpan(int dimension, int order, float u, 
                             const float knots[]);
    static void     basisFuns(int span, float u, int order,
                              const float knots[], float basis[], 
                              float deriv[]);
    static Vec3f    curvePoint(int dimension, int order, 
                               const float knots[],
                               const Vec3f controlPoints[],
                               const float weight[], float u);
    virtual void    setHandle(MFVec3f *newValue, int handle, float newWeight,
                              const Vec3f &newV, const Vec3f &oldV,
                              bool already_changed = false);
    virtual void    setHandle(float newWeight, 
                              const Vec3f &newV, const Vec3f &oldV);
private:
    int             _dimension;
    bool            _isInternal;
    bool            _x3d;
};

#endif // _NODE_NURBS_CURVE_H
