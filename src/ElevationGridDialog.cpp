/*
 * ElevationGridDialog.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"
#include "ElevationGridDialog.h"
#include <stdio.h>
#include <stdlib.h>
#include "swt.h"
#include "resource.h"

ElevationGridDialog::ElevationGridDialog(SWND parent, int width, int depth)
  : Dialog(parent, IDD_ELEVATION_GRID)
{
    _width = width;
    _depth = depth;
    LoadData();
}

ElevationGridDialog::~ElevationGridDialog()
{
}

void
ElevationGridDialog::SaveData()
{
    char buf[128];

    swGetText(swGetDialogItem(_dlg, IDC_WIDTH), buf, 128);
    _width = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_DEPTH), buf, 128);
    _depth = atoi(buf);
}

bool
ElevationGridDialog::Validate()
{
    return _width > 0 && _depth > 0;
}

void
ElevationGridDialog::LoadData()
{
    char buf[128];

    mysnprintf(buf, 128, "%d", _width);
    swSetText(swGetDialogItem(_dlg, IDC_WIDTH), buf);
    mysnprintf(buf, 128, "%d", _depth);
    swSetText(swGetDialogItem(_dlg, IDC_DEPTH), buf);
}

