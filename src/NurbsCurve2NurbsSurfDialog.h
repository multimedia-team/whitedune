/*
 * NurbsCurve2NurbsSurfDialog.h
 *
 * Copyright (C) 2003 Th. Rothermel
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "Node.h"
#include "Vec3f.h"

#ifndef _DIALOG_H
#include "Dialog.h"
#endif

class NurbsCurve2NurbsSurfDialog : public Dialog {
public:
                 NurbsCurve2NurbsSurfDialog(SWND parent, Node *node,
                                            int narcs, int uDegree, 
                                            float rDegree, int method);
    int          getNarcs() {return _narcs;}
    int          getuDegree() {return _uDegree;}
    float        getrDegree() {return _rDegree;}             
    Vec3f        getP1() {return _P1;}
    Vec3f        getP2() {return _P2;}
    int          getMethod() {return _method; }
    bool         getFlatten() { return _flatten; }

    virtual void SaveData();
    virtual bool Validate();
    virtual void LoadData();

protected:

    int          _method;
    int          _narcs;
    int          _uDegree;
    float        _rDegree;
    Vec3f        _P1;
    Vec3f        _P2;
    bool         _flatten;
    Node        *_node;
};
