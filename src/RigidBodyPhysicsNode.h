/*
 * RigidBodyPhysicsNode.h
 *
 * 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _RIGID_BODY_PHYSICS_NODE_H
#define _RIGID_BODY_PHYSICS_NODE_H

#ifndef _NODE_H
#include "Node.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif

#include "SFMFTypes.h"

#include "ode_include.h"

class RigidBodyPhysicsNode : public Node {
public:
                    RigidBodyPhysicsNode(Scene *scene, Proto *def) : 
                          Node(scene, def) {}

    virtual int     writeProto(int filedes) 
                       {
                       return ((Node *)this)->writeProto(filedes, 
                                    "urn:web3d:x3d:node:",
                                    "rigidBodyPhysicsNodes"
#ifdef HAVE_RIGID_BODY_PHYSICS_NODES_PROTO_URL
                                    , HAVE_RIGID_BODY_PHYSICS_NODES_PROTO_URL
#endif
                                                              );
                       }

    virtual bool    isRigidBodyPhysicsNode(void) { return true; }

#ifdef HAVE_LIBODE
   /**
    * This node is about to be deleted due to a change in loaded world. 
    * Clear up the ODE resources in use.
    */
    virtual void    deleteFromODE(void) {}

    virtual void    evaluateCollisions(void) {}

   /**
    * Instruct the group node to evaluate itself right now based on the given
    * time delta from the last time this was evaluated. Does not check to see
    * if the model is enabled first. That should be done by the user code.
    */
    virtual void    evaluateModel(void) {}

   /**
    * Update everything from ODE, back into the node fields. This is done at
    * the start of the next frame so that all the events, listeners etc fire
    * at the right time in the event model.
    */
    virtual void    updatePostSimulation(void) {}

    virtual void    updateContacts(void) {}

   /**
    * Go through the list of input contacts, process them and send them off
    * to ODE.
    */
    virtual void    processInputContacts(void) {}
   

   /**
    * Is this group enabled for use right now?
    *
    * @return true if this is enabled
    */
    virtual bool    isEnabled() { return false; }

   /**
    * Adjust the model's timestep to the new value (in seconds). This is
    * called periodically to adjust the timestep made based on the current
    * frame rate to adjust the model.
    *
    * @param deltaT The time change in seconds
    */
    virtual void   setTimestep(float deltaT) {}

#endif
};

#endif

