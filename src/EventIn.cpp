/*
 * EventIn.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "EventIn.h"
#include "Scene.h"
#include "FieldValue.h"
#include "DuneApp.h"

EventIn::EventIn(int type, const MyString& name, int flags, 
                 ExposedField *exposedField, const MyString &x3dName) 
{
    _type = type;
    _name = name;
    _x3dName = x3dName;
    _flags = flags;
    _exposedField = exposedField;
    _field = -1;
}

EventIn::~EventIn()
{
}

const char *EventIn::getElementName(bool x3d) const
{
    if (x3d)
        return "inputOnly";
    else
        return "eventIn";
}

int EventIn::write(int f, int indent, int flags) const
{
    if (_exposedField) return 0;

    int ind = indent;
    if (isX3dXml(flags))
        ind += TheApp->GetIndent();
    RET_ONERROR( writeElementPart(f, ind, flags) )

    if (isX3dXml(flags))
        RET_ONERROR( mywritestr(f, "/>\n") )    
    else
        RET_ONERROR( mywritestr(f, "\n") )    
    TheApp->incSelectionLinenumber();
    return 0;
}
