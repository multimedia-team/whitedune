/*
 * MFBoolItem.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "MFBoolItem.h"
#include "MFBool.h"
#include "SFBoolItem.h"
#include "SFBool.h"
#include "FieldView.h"
#include "swt.h"

MFBoolItem::MFBoolItem(FieldView *view) : MFieldViewItem(view)
{
}

FieldViewItem *
MFBoolItem::CreateSFItem()
{
    return new SFBoolItem(_view);
}

//
// when an item is collapsed, draw the 1st element
//

void
MFBoolItem::Draw(SDC dc, int x, int y)
{
    int index = _index;
    if (IsCollapsed()) {
        if (((MFBool *) _value)->getSize() == 1) {
            char buf[128];
            if (((MFBool *) _value)->getValue(0))
                strcpy(buf, "TRUE");
            else
                strcpy(buf, "FALSE");
            swDrawText(dc, x, y + _view->GetItemHeight() - 3, buf);
        }
    } else {

    }
}

void
MFBoolItem::InsertSFValue(int index)
{
    ((MFBool *)_value)->insertSFValue(index, false);
}

void                
MFBoolItem::RemoveSFValue(int index) 
{
    ((MFBool *)_value)->removeSFValue(index);
}

void
MFBoolItem::OnClicked(int offset)
{    
    MFBool *v = (MFBool *)_value;
    InitIndexValue(0, _value);
    if (!IsCollapsed() || (v->getSize() == 0)) {
        InsertSFValue(0);
        _children.insert(new FieldViewItem(_view), 0); 
    } else {
        bool value = v->getValue(0);
        v->setSFValue(offset, !value);
        InitIndexValue(offset, v);
    }
}

