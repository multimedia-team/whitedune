/*
 * NodeContour2D.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2004 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeContour2D.h"
#include "Proto.h"
#include "FieldValue.h"
#include "MFNode.h"
#include "SFVec3f.h"
#include "SFFloat.h"
#include "Scene.h"

ProtoContour2D::ProtoContour2D(Scene *scene)
  : Proto(scene, "Contour2D")
{                    
    addEventIn(MFNODE, "addChildren");
    addEventIn(MFNODE, "removeChildren");

    children.set(
          addExposedField(MFNODE, "children", new MFNode(), 
                          NURBS_CONTROL_CURVE_NODE));
}

Node *
ProtoContour2D::create(Scene *scene)
{ 
    return new NodeContour2D(scene, this); 
}

NodeContour2D::NodeContour2D(Scene *scene, Proto *def)
  : Node(scene, def)
{
}

int
NodeContour2D::writeProto(int f)
{
    RET_ONERROR( mywritestr(f ,"EXTERNPROTO Contour2D[\n") )    
    TheApp->incSelectionLinenumber();
    RET_ONERROR( writeProtoArguments(f) )
    RET_ONERROR( mywritestr(f ," ]\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ,"[\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:web3d:vrml97:node:Contour2D\",\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:inet:blaxxun.com:node:Contour2D\",\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:ParaGraph:Contour2D\",\n") )
    TheApp->incSelectionLinenumber();
#ifdef HAVE_VRML97_AMENDMENT1_PROTO_URL
    RET_ONERROR( mywritestr(f ," \"") )
    RET_ONERROR( mywritestr(f ,HAVE_VRML97_AMENDMENT1_PROTO_URL) )
    RET_ONERROR( mywritestr(f ,"/Contour2DPROTO.wrl") )
    RET_ONERROR( mywritestr(f ,"\"\n") )
    TheApp->incSelectionLinenumber();
#else
    RET_ONERROR( mywritestr(f ," \"Contour2DPROTO.wrl\",\n") )
    TheApp->incSelectionLinenumber();
#endif
    RET_ONERROR( mywritestr(f ," \"http://129.69.35.12/dune/docs/vrml97Amendment1/Contour2DPROTO.wrl\"\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ,"]\n") )
    TheApp->incSelectionLinenumber();
    return 0;
}

int             
NodeContour2D::write(int filedes, int indent)
{
    if (!_scene->isPureVRML())
        RET_ONERROR( NodeData::write(filedes, indent) )
    return 0;
}

int
NodeContour2D::getComponentLevel(void) const
{
    return 4;
}

const char* 
NodeContour2D::getComponentName(void) const
{
    static const char* name = "NURBS";
    return name;
}

