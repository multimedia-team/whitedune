/*
 * Box2NurbsDialog.h
 *
 * Copyright (C) 1999 Stephen F. White, 2003 Thomas Rothermel
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef BOX2NURBS_DIALOG_H
#define BOX2NURBS_DIALOG_H

#ifndef _DIALOG_H
#include "Dialog.h"
#endif

#include "swttypedef.h"

#include "Node.h"

class Box2NurbsDialog : public Dialog 
{
public:
                        Box2NurbsDialog(SWND parent, 
                                        int nuAreaPoints,
                                        int nvAreaPoints,
                                        int nzAreaPoints,
                                        int uDegree, int vDegree);
    virtual            ~Box2NurbsDialog();

    void                LoadData();
    virtual void        SaveData();
    virtual bool        Validate();
    int                 getHave6Planes() {return _have6Planes; }
    int                 getNuAreaPoints() {return _nuAreaPoints;}
    int                 getNvAreaPoints() {return _nvAreaPoints;}
    int                 getNzAreaPoints() {return _nzAreaPoints;}
    int                 getuDegree() {return _uDegree;}
    int                 getvDegree() {return _vDegree;}
private:
    bool                _have6Planes;
    int                 _nuAreaPoints;
    int                 _nvAreaPoints;
    int                 _nzAreaPoints;
    int                 _uDegree;
    int                 _vDegree;
};

#endif
