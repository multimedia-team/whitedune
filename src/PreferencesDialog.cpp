/*
 * PreferencesDialog.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"
#include "PreferencesDialog.h"
#include "DuneApp.h"
#include "resource.h"
#include "swt.h"
#include "EulerAngles.h"

static int rot[24] = {
    EulOrdXYZs, EulOrdXYXs, EulOrdXZYs, EulOrdXZXs, 
    EulOrdYZXs, EulOrdYZYs, EulOrdYXZs, EulOrdYXYs,
    EulOrdZXYs, EulOrdZXZs, EulOrdZYXs, EulOrdZYZs,

    EulOrdZYXr, EulOrdXYXr, EulOrdYZXr, EulOrdXZXr,
    EulOrdXZYr, EulOrdYZYr, EulOrdZXYr, EulOrdYXYr,
    EulOrdYXZr, EulOrdZXZr, EulOrdXYZr, EulOrdZYZr 
};

static const char *rotTitles[24] = { 
    "XYZs", "XYXs", "XZYs", "XZXs",
    "YZXs", "YZYs", "YXZs", "YXYs",
    "ZXYs", "ZXZs", "ZYXs", "ZYZs",

    "ZYXr", "XYXr", "YZXr", "XZXr",
    "XZYr", "YZYr", "ZXYr", "YXYr",
    "YXZr", "ZXZr", "XYZr", "ZYZr"
};

void
PreferencesDialog::OnCommand(int id)
{
    if (id == IDOK) {
        SaveData();
        if (Validate()) {
            swEndDialog(IDOK);
        }
    } else if (id == IDC_PREF_DEFAULTS) {
        TheApp->PreferencesDefaults();
        LoadData();
    } else if (id == IDCANCEL) {
        swEndDialog(IDCANCEL);
    }
}

PreferencesDialog::PreferencesDialog(SWND parent)
  : Dialog(parent, IDD_PREFERENCES)
{
    LoadData();
}

void PreferencesDialog::LoadData()
{
    char buf[128];
    int handleMode = TheApp->GetHandleMode();
    swSetCheck(swGetDialogItem(_dlg, IDC_HANDLES_NONE),
               handleMode == HM_NONE);
    swSetCheck(swGetDialogItem(_dlg, IDC_HANDLES_SELECTED),
               handleMode == HM_SELECTED);
    swSetCheck(swGetDialogItem(_dlg, IDC_HANDLES_TREE),
               handleMode == HM_TREE);
    swSetCheck(swGetDialogItem(_dlg, IDC_HANDLES_ALL),
               handleMode == HM_ALL);
    swSetCheck(swGetDialogItem(_dlg, IDC_SHOW_ALL_FIELDS),
               TheApp->GetShowAllFields());
    swSetCheck(swGetDialogItem(_dlg, IDC_HANDLE_MESH_ALWAYS),
               TheApp->GetHandleMeshAlways());

    mysnprintf(buf, 128, "%g", TheApp->GetPreferenceHandleSize());
    swSetText(swGetDialogItem(_dlg, IDC_HANDLE_SIZE), buf);
    mysnprintf(buf, 128, "%g", TheApp->GetHandleScale());
    swSetText(swGetDialogItem(_dlg, IDC_HANDLE_SCALE), buf);
    mysnprintf(buf, 128, "%g", TheApp->GetPointSetSize());
    swSetText(swGetDialogItem(_dlg, IDC_POINTSET_SIZE), buf);
    SWND combo = swGetDialogItem(_dlg, IDC_ROTATION_ORDER);
    swComboBoxDeleteAll(combo);
    for (int i = 0; i < 24; i++) {
        swComboBoxAppendItem(combo, rotTitles[i]);
        if (rot[i] == TheApp->GetRotationOrder()) {
            swComboBoxSetSelection(combo, i);
        }
    }
    mysnprintf(buf, 128, "%g", TheApp->GetHandleEpsilon());
    swSetText(swGetDialogItem(_dlg, IDC_HANDLE_EPSILON), buf);
    swSetCheck(swGetDialogItem(_dlg, IDC_KEEP_URLS),
               TheApp->GetKeepURLs());
    mysnprintf(buf, 128, "%g", TheApp->GetNearClippingPlaneDist());
    swSetText(swGetDialogItem(_dlg, IDC_NEAR_CLIPPING_PLANE_DIST), buf);
    mysnprintf(buf, 128, "%g", TheApp->GetFarClippingPlaneDist());
    swSetText(swGetDialogItem(_dlg, IDC_FAR_CLIPPING_PLANE_DIST), buf);
    mysnprintf(buf, 128, "%d", TheApp->GetMaxInlinesToLoad());
    swSetText(swGetDialogItem(_dlg, IDC_MAX_INLINES_TO_LOAD), buf);
    mysnprintf(buf, 128, "%d", TheApp->GetMaxKeysInChannelView());
    swSetText(swGetDialogItem(_dlg, IDC_MAX_KEYS_IN_CHANNELVIEW), buf);
}

bool
PreferencesDialog::Validate()
{
    char buf[128];
    swGetText(swGetDialogItem(_dlg, IDC_NEAR_CLIPPING_PLANE_DIST), buf, 128);
    float nearClippingPlane = atof(buf);
    if (nearClippingPlane <= 0) {
        TheApp->MessageBoxId(IDS_NEAR_CLIPPING_TOO_SMALL);
        return false;
    }
    swGetText(swGetDialogItem(_dlg, IDC_FAR_CLIPPING_PLANE_DIST), buf, 128);
    float FarClippingPlane = atof(buf);
    if (nearClippingPlane >= FarClippingPlane) {
        TheApp->MessageBoxId(IDS_NEAR_BIGGER_FAR);
        return false;
    }
    return true;
}


void
PreferencesDialog::SaveData() 
{
    HandleMode handleMode;
    SWND saf;
    char buf[128];

    if (swGetCheck(swGetDialogItem(_dlg, IDC_HANDLES_NONE))) {
        handleMode = HM_NONE;
    } else if (swGetCheck(swGetDialogItem(_dlg, IDC_HANDLES_SELECTED))) {
        handleMode = HM_SELECTED;
    } else if (swGetCheck(swGetDialogItem(_dlg, IDC_HANDLES_TREE))) {
        handleMode = HM_TREE;
    } else if (swGetCheck(swGetDialogItem(_dlg, IDC_HANDLES_ALL))) {
        handleMode = HM_ALL;
    }
    TheApp->SetHandleMeshAlways(swGetCheck(swGetDialogItem(_dlg, IDC_HANDLE_MESH_ALWAYS)));
    TheApp->SetHandleMode(handleMode);
    swGetText(swGetDialogItem(_dlg, IDC_HANDLE_SIZE), buf, 128);
    TheApp->SetPreferenceHandleSize(atof(buf));    
    swGetText(swGetDialogItem(_dlg, IDC_HANDLE_SCALE), buf, 128);
    TheApp->SetPreferenceHandleScale(atof(buf));    
    swGetText(swGetDialogItem(_dlg, IDC_POINTSET_SIZE), buf, 128);
    TheApp->SetPointSetSize(atof(buf));
    saf = swGetDialogItem(_dlg, IDC_SHOW_ALL_FIELDS);
    TheApp->SetShowAllFields(swGetCheck(saf) != FALSE);
    int sel = swComboBoxGetSelection(swGetDialogItem(_dlg, IDC_ROTATION_ORDER));
    TheApp->SetRotationOrder(rot[sel]);
    TheApp->SetRotationTitle(rotTitles[sel]);
    swGetText(swGetDialogItem(_dlg, IDC_HANDLE_EPSILON), buf, 128);
    TheApp->SetHandleEpsilon(atof(buf));
    swGetText(swGetDialogItem(_dlg, IDC_NEAR_CLIPPING_PLANE_DIST), buf, 128);
    TheApp->SetNearClippingPlaneDist(atof(buf));
    swGetText(swGetDialogItem(_dlg, IDC_FAR_CLIPPING_PLANE_DIST), buf, 128);
    TheApp->SetFarClippingPlaneDist(atof(buf));
    saf = swGetDialogItem(_dlg, IDC_KEEP_URLS);
    TheApp->SetKeepURLs(swGetCheck(saf) != FALSE);
    swGetText(swGetDialogItem(_dlg, IDC_MAX_INLINES_TO_LOAD), buf, 128);
    TheApp->SetMaxInlinesToLoad(atoi(buf));
    swGetText(swGetDialogItem(_dlg, IDC_MAX_KEYS_IN_CHANNELVIEW), buf, 128);
    TheApp->SetMaxKeysInChannelView(atoi(buf));
}
