/*
 * NodeHAnimHumanoid.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"
#include <sys/types.h>
#include <sys/stat.h>
#ifndef _WIN32
# include <fcntl.h>
#endif

#include "swt.h"
#include "NodeHAnimHumanoid.h"
#include "Scene.h"
#include "Proto.h"
#include "FieldValue.h"
#include "SFVec3f.h"
#include "SFRotation.h"
#include "SFFloat.h"
#include "MFNode.h"
#include "FieldCommand.h"
#include "Matrix.h"
#include "Util.h"
#include "NodeViewpoint.h"
#include "NodeNavigationInfo.h"
#include "Field.h"
#include "ExposedField.h"

#include "RenderState.h"

void ProtoHAnimHumanoid::addElements(void) 
{
    info.set (
           addExposedField(MFSTRING, "info", new MFString()));
    joints.set (
           addExposedField(MFNODE, "joints", new MFNode(), X3D_HANIM_JOINT));
    name.set(
           addExposedField(SFSTRING, "name", new SFString()));
    segments.set (
           addExposedField(MFNODE, "segments", new MFNode(), X3D_HANIM_SEGMENT));
    sites.set (
           addExposedField(MFNODE, "sites", new MFNode(), X3D_HANIM_SITE));
    skeleton.set(
           addExposedField(MFNODE, "skeleton", new MFNode(), X3D_HANIM_JOINT));
    skin.set(
           addExposedField(MFNODE, "skin", new MFNode(), CHILD_NODE));
    skinCoord.set(
           addExposedField(SFNODE, "skinCoord", new SFNode(), COORDINATE_NODE));
    skinNormal.set(
           addExposedField(SFNODE, "skinNormal", new SFNode(), VRML_NORMAL));
    version.set(
           addExposedField(SFSTRING, "version", new SFString()));
    viewpoints.set(
           addExposedField(MFNODE, "viewpoints", new MFNode(), VRML_VIEWPOINT));
}

ProtoHAnimHumanoid::ProtoHAnimHumanoid(Scene *scene) 
  : TransformProto(scene, "HAnimHumanoid")
{                    
     addElements();     
}

Node *
ProtoHAnimHumanoid::create(Scene *scene)
{ 
    return new NodeHAnimHumanoid(scene, this); 
}

NodeHAnimHumanoid::NodeHAnimHumanoid(Scene *scene, Proto *def)
  : TransformNode(scene, def)
{
}

NodeHAnimHumanoid::~NodeHAnimHumanoid()
{
}

int
NodeHAnimHumanoid::getComponentLevel(void) const
{
    return 1;
}

const char* 
NodeHAnimHumanoid::getComponentName(void) const
{
    static const char* name = "H-Anim";
    return name;
}

void
NodeHAnimHumanoid::drawHandles()
{
    if (isInsideProto())
        return;

    _handleScale = TheApp->GetHandleScale() *
                   _scene->getNavigationInfo()->speed()->getValue();

    RenderState state;
                   
    TransformMode* tm=_scene->getTransformMode();
    if (tm->tmode==TM_TRANSLATE)
       state.drawTranslationHandles(this, _handleScale);
    else if (tm->tmode==TM_6D)
       state.draw6DHandles(this, _handleScale);
    else if (tm->tmode==TM_6DLOCAL)
       state.draw6DlocalHandles(this, _handleScale);
    else if (tm->tmode==TM_ROCKET)
       state.drawRocketHandles(this, _handleScale);
    else if (tm->tmode==TM_HOVER)
       state.drawHoverHandles(this, _handleScale);
    else if (tm->tmode==TM_ROTATE)
       state.drawRotationHandles(this, _handleScale);
    else if (tm->tmode==TM_SCALE)
       state.drawScaleHandles(this, _handleScale, scale()->getValue());
    else if (tm->tmode==TM_UNIFORM_SCALE)
       state.drawUniformScaleHandles(this, _handleScale, scale()->getValue());
    else if (tm->tmode==TM_CENTER)
       state.drawCenterHandles(this, _handleScale);
}

Vec3f
NodeHAnimHumanoid::getHandle(int handle, int *constraint, int *field)
{
    const float *fscale = scale()->getValue();
    SFRotation *sfrotation = rotation();

    switch (handle) {
      case TRANSLATION_X:
        *constraint = CONSTRAIN_X;
        *field = translation_Field();
        return Vec3f(HANDLE_SIZE, 0.0f, 0.0f);
      case TRANSLATION_Y:
        *constraint = CONSTRAIN_Y;
        *field = translation_Field();
        return Vec3f(0.0f, HANDLE_SIZE, 0.0f);
      case TRANSLATION_Z:
        *constraint = CONSTRAIN_Z;
        *field = translation_Field();
        return Vec3f(0.0f, 0.0f, HANDLE_SIZE);
      case TRANSLATION:
        *field = translation_Field();
        return Vec3f(0.0f, 0.0f, 0.0f);
      case ROTATION:
        *field = rotation_Field();
        *constraint = CONSTRAIN_SPHERE;
        return sfrotation->getEulerAngles(0);
      case ROTATION_X:
        *field = rotation_Field();
        *constraint = CONSTRAIN_YZ;
        return Vec3f(sfrotation->getEulerAngles(0).x, 0.0f, 0.0f);
      case ROTATION_Y:
        *field = rotation_Field();
        *constraint = CONSTRAIN_ZX;
        return Vec3f(0.0f, sfrotation->getEulerAngles(0).y, 0.0f);
      case ROTATION_Z:
        *field = rotation_Field();
        *constraint = CONSTRAIN_XY;
        return Vec3f(0.0f, 0.0f, sfrotation->getEulerAngles(0).z);
      case SCALE_X:
      case UNIFORM_SCALE_X:
        *constraint = CONSTRAIN_X;
        *field = scale_Field();
        return Vec3f(fscale[0] * HANDLE_SIZE, 0.0f, 0.0f);
      case SCALE_Y:
      case UNIFORM_SCALE_Y:
        *constraint = CONSTRAIN_Y;
        *field = scale_Field();
        return Vec3f(0.0f, fscale[1] * HANDLE_SIZE, 0.0f);
      case SCALE_Z:
      case UNIFORM_SCALE_Z:
        *constraint = CONSTRAIN_Z;
        *field = scale_Field();
        return Vec3f(0.0f, 0.0f, fscale[2] * HANDLE_SIZE);
      case CENTER_X:
        *constraint = CONSTRAIN_X;
        *field = center_Field();
        return Vec3f(HANDLE_SIZE, 0.0f, 0.0f);
      case CENTER_Y:
        *constraint = CONSTRAIN_Y;
        *field = center_Field();
        return Vec3f(0.0f, HANDLE_SIZE, 0.0f);
      case CENTER_Z:
        *constraint = CONSTRAIN_Z;
        *field = center_Field();
        return Vec3f(0.0f, 0.0f, HANDLE_SIZE);
      case CENTER:
        *constraint = CONSTRAIN_NONE;
        *field = center_Field();
        return Vec3f(0.0f, 0.0f, 0.0f);
      default:
        *field = scale_Field();
        return Vec3f(fscale);
    }
}

void
NodeHAnimHumanoid::setHandle(int handle, const Vec3f &v)
{
    const float *fcenter = center()->getValue();
    SFRotation *sfrotation = rotation();
    const float *rot = sfrotation->getValue();
    const float *fscale = scale()->getValue();
    const float *ftranslation = translation()->getValue();
    Matrix mat;

    glPushMatrix();
    glLoadIdentity();
    if ((handle==CENTER_X) || (handle==CENTER_Y) || (handle==CENTER_Z) ||
        (handle==CENTER))
       glTranslatef(fcenter[0], fcenter[1], fcenter[2]);
    else
       glTranslatef(ftranslation[0], ftranslation[1], ftranslation[2]);
    glRotatef(RAD2DEG(rot[3]), rot[0], rot[1], rot[2]);
    glGetFloatv(GL_MODELVIEW_MATRIX, mat);
    glPopMatrix();

    ProtoHAnimHumanoid *proto = (ProtoHAnimHumanoid *)getProto();


    switch (handle) {
      case TRANSLATION:
        {
        Vec3f vec(_scene->constrainVec(v));
        _scene->setField(this, proto->translation, new SFVec3f(mat * vec));
        }
        break;
      case TRANSLATION_X:
        _scene->setField(this, proto->translation, 
              new SFVec3f(mat * (v - Vec3f(HANDLE_SIZE, 0.0f, 0.0f))));
        break;
      case TRANSLATION_Y:
        _scene->setField(this, proto->translation, 
              new SFVec3f(mat * (v - Vec3f(0.0f, HANDLE_SIZE, 0.0f))));
        break;
      case TRANSLATION_Z:
        _scene->setField(this, proto->translation, 
              new SFVec3f(mat * (v - Vec3f(0.0f, 0.0f, HANDLE_SIZE))));
        break;
      case ROTATION:
        _scene->setField(this, proto->rotation, new SFRotation(v, 0));
        break;
      case ROTATION_X:
        _scene->setField(this, proto->rotation, 
              new SFRotation(Quaternion(Vec3f(1.0f, 0.0f, 0.0f), v.x) * 
                             sfrotation->getQuat()));
        break;
      case ROTATION_Y:
        _scene->setField(this, proto->rotation, 
              new SFRotation(Quaternion(Vec3f(0.0f, 1.0f, 0.0f), v.y) * 
                             sfrotation->getQuat()));
        break;
      case ROTATION_Z:
        _scene->setField(this, proto->rotation, 
              new SFRotation(Quaternion(Vec3f(0.0f, 0.0f, 1.0f), v.z) * 
                             sfrotation->getQuat()));
        /*
        _scene->setField(this, proto->rotation, 
              new SFRotation(sfrotation->getQuat() 
                             * Quaternion(Vec3f(0.0f, 0.0f, 1.0f), v.x) *
                             sfrotation->getQuat().conj()));
        */
        break;
      case SCALE_X:
        if (v.x > 0.0f) {
            _scene->setField(this, proto->scale, 
                             new SFVec3f(Vec3f(v.x, fscale[1], fscale[2])));
        }
        break;
      case SCALE_Y:
        if (v.y > 0.0f) {
            _scene->setField(this, proto->scale, 
                             new SFVec3f(Vec3f(fscale[0], v.y, fscale[2])));
        }
        break;
      case SCALE_Z:
        if (v.z > 0.0f) {
            _scene->setField(this, proto->scale, 
                             new SFVec3f(Vec3f(fscale[0], fscale[1], v.z)));
        }
        break;
      case UNIFORM_SCALE_X:
        if (v.x > 0.0f) {
            float mult = v.x / fscale[0];
            Vec3f vMult(v.x, fscale[1] * mult, fscale[2] * mult);
            _scene->setField(this, proto->scale, 
                             new SFVec3f(vMult.x, vMult.y, vMult.z));
        }
        break;
      case UNIFORM_SCALE_Y:
        if (v.y > 0.0f) {
            float mult = v.y / fscale[1];
            Vec3f vMult(fscale[0] * mult, v.y, fscale[2] * mult);
            _scene->setField(this, proto->scale, 
                             new SFVec3f(vMult.x, vMult.y, vMult.z));
        }
        break;
      case UNIFORM_SCALE_Z:
        if (v.z > 0.0f) {
            float mult = v.z / fscale[2];
            Vec3f vMult(fscale[0] * mult, fscale[1] * mult, v.z);
            _scene->setField(this, proto->scale, 
                             new SFVec3f(vMult.x, vMult.y, vMult.z));
        }
        break;
      case SCALE:
        if ((v.x > 0.0f) && (v.y > 0.0f) && (v.z > 0.0f)) {
            _scene->setField(this, proto->scale, 
                             new SFVec3f(Vec3f(v.x,v.y,v.z)));
        }
        break;
      case CENTER:
        _scene->setField(this, proto->center, new SFVec3f(mat * v));
        break;
      case CENTER_X:
        _scene->setField(this, proto->center, 
              new SFVec3f(mat * (v - Vec3f(HANDLE_SIZE, 0.0f, 0.0f))));
        break;
      case CENTER_Y:
        _scene->setField(this, proto->center, 
              new SFVec3f(mat * (v - Vec3f(0.0f, HANDLE_SIZE, 0.0f))));
        break;
      case CENTER_Z:
        _scene->setField(this, proto->center, 
              new SFVec3f(mat * (v - Vec3f(0.0f, 0.0f, HANDLE_SIZE))));
        break;
      default:
        assert(0);
        break;
    }
}

