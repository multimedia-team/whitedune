/*
 * NodeGroup.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_GROUP_H
#define _NODE_GROUP_H

#ifndef _NODE_H
#include "Node.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif

#include "SFMFTypes.h"
#include "NodeStaticGroup.h"

class NodeNurbsGroup;
class NodeNurbsSet;
class NodeAnchor;

class ProtoGroup : public ProtoStaticGroup {
public:
                    ProtoGroup(Scene *scene);
                    ProtoGroup(Scene *scene, const char *name);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return VRML_GROUP; }

    void            addElements(void);
};

class NodeGroup : public NodeStaticGroup {
public:
                    NodeGroup(Scene *scene, Proto *proto);
                    NodeGroup(NodeNurbsGroup *nurbsGroup);
                    NodeGroup(NodeNurbsSet *nurbsSet);
                    NodeGroup(NodeAnchor *anchor);

    virtual int     getProfile(void) const;
    virtual Node   *copy() const { return new NodeGroup(*this); }

    NodeList       *buildVrml97Children(MFNode *children);
};

#endif // _NODE_GROUP_H
