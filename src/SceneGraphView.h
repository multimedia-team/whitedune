/*
 * SceneGraphView.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _SCENE_GRAPH_VIEW_H
#define _SCENE_GRAPH_VIEW_H

class NodeList;
class Node;
class DuneDoc;
class FieldValue;

#ifndef _SWT_H
#include "swt.h"
#endif

#ifndef _SCENE_VIEW_H
#include "SceneView.h"
#endif

#ifndef _POINT_H
#include "Point.h"
#endif

#include "Types.h"

class SceneGraphView : public SceneView
{
public:
        SceneGraphView(Scene *scene, SWND parent);
        virtual ~SceneGraphView();

public:
        virtual void    OnUpdate(SceneView *sender, int type, Hint *hint);
        virtual void    OnLButtonDown(int x, int y, int modifiers);
        virtual void    OnLButtonUp(int x, int y, int modifiers);
        virtual void    OnMouseMove(int x, int y, int modifiers);
        void            DoMouseMove(int px, int py, int modifiers);

        virtual void    OnDraw(int x, int y, int width, int height);
        virtual void    OnSize(int width, int height);
        virtual void    OnMouseLeave();

        const char     *getName(void) const { return "SceneGraphView"; }

        void            Initialize();

        typedef enum { 
            NONE, 
            ROUTING, 
            TRACKING, 
            CUTTING 
        } SGMode;
    

        void            Position(Node *node);
        void            accountGraphSize(Node *node);
        void            accountGraphPosition(Node *node);

        void            YPosition(int width, int height);
        int             GetYEndRouteBlock(void);

        void            DrawNode(SDC dc, Node *node, int xPos, int yPos);
        Node           *HitTest(int x, int y) const;
        int             SocketHitTest(int x, int y, Node *node, int *side) 
                              const;
        void            GetItemRect(Node *node, int *x, int *y, int *width, 
                                    int *height);

        void            DrawRoute(SDC dc, Point start, Point end, int color);
        void            InvalidateRoute(Point start, Point end);
        void            DrawRoutes(SDC dc, Node *node);
        void            DrawSocketName(SDC dc, int x, int y, const char *name,
                                       bool rightAlign);
        void            InvalidateNode(Node *node);
        void            InvalidateNodeRec(Node *node);
        Point           GetSocketPosition(Node *node, int socket, int side) const;
        void            HighlightSocket(Point point);
        void            DoMouseMove(int x, int y);
        void            CheckAutoScroll(int x, int y);
        SBITMAP         LoadSocketBitmap(int id, int c);
        void            BuildSocketBitmaps();
        void            CutRoutes(const Point &start, const Point &end);
        bool            IntersectRoute(Point start, Point end,
                                       Point rStart, Point rEnd);
        int             OnTimer();
        void            zoomIn(void);
        void            zoomOut(void);
        void            unZoom(void);
        void            jumpToSelection(void);
        bool            canZoomIn(void) { return (_zoom < 1.0); }

        void            moveToTop(Node *node, Node* ignoreNode = NULL);
        void            moveRoutesToTop(Node *moveNode);

protected:
        void            getZoomGraphPosition(int &x, int &y, Node* node) const;
        void            setZoomGraphPosition(Node *node, int x, int y);
        void            getZoomGraphSize(int &width, int &height, Node *node) const;
        void            setGraphSize(Node *node, int width, int height);
        void            setLastXPosition(int x);
        void            setLastYPosition(int y);
        void            warnFlooded(void);
        int             getFirstYPosition(void);
        void            SetScrollSizes(int width, int height);
        void            SetSize(int width, int height);
        void            moveToTop(Node *moveNode, int x, 
                                  Node *ignoreNode = NULL);

protected:
        SWND            _window;
        SWND            _scroller;

        int             _lastXPosition;
        int             _lastYPosition;
        int             _maxYPosition;

        int             _XNodesOnlyEventOuts; 
        int             _YNodesOnlyEventOuts; 

        int             _XNodesBothEvents; 
        int             _YNodesBothEvents; 

        int             _XNodesOnlyEventIns;
        int             _YNodesOnlyEventIns;

        SGMode          _mode;
        Point           _point1;
        Point           _point2;
        Node           *_dstNode;
        int             _dstSocket;
        int             _dstSide;
        Node           *_srcNode;
        int             _srcSocket;
        int             _srcSide;
        SFONT           _font;
        SBITMAP         _socketBitmaps[LAST_TYPE + 2];
        SBITMAP         _nodeBitmap;
        SBITMAP         _socketBitmap;
        SBITMAP         _socketBitmapRecommended;
        int             _typeColors[LAST_TYPE + 2];
        int             _autoScrolling;
        int             _autoScrollPX, _autoScrollPY;
        int             _autoScrollDX, _autoScrollDY;
        int             _highlight;
        int             _shadow;
        int             _face;
        STIMER          _timer;
        float           _zoom;
        bool            _floodedWarning;
};

#endif // _SCENE_GRAPH_VIEW_H
