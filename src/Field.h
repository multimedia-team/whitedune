/*
 * Field.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _FIELD_H
#define _FIELD_H

#ifndef _ELEMENT_H
# include "Element.h"
#endif
#ifndef _NODE_H   
# include "Node.h"   
#endif


class FieldValue;
class ExposedField;

class Field : public Element {
public:
                    Field(int type, 
                          const MyString &name, FieldValue *value,
                          ExposedField *exposedField = NULL,
                          FieldValue *min = NULL, FieldValue *max = NULL,
                          int nodeType = ANY_NODE, int flags = 0, 
                          const char **strings = NULL,
                          const MyString &x3dName = "");
    virtual        ~Field();
    virtual int     getElementType() const { return EL_FIELD; }
    virtual const char *getElementName(bool x3d) const;
    virtual int     write(int filedes, int indent, int flags = 0) const;
    FieldValue     *getDefault(bool x3d) const 
                       { 
                       if (x3d && (_x3dValue != NULL))
                           return _x3dValue; 
                       return _value; 
                       }
    ExposedField   *getExposedField() const { return _exposedField; }
    FieldValue     *getMin() const { return _min; }
    FieldValue     *getMax() const { return _max; }
    int             getNodeType() const { return _nodeType; }
    void            addToNodeType(int nodeType);
    const char    **getStrings() const { return _strings; }
    int             getEventIn() { return _eventIn; }
    void            setEventIn(int eventIn) { _eventIn = eventIn; } 
    void            addX3dDefault(FieldValue *x3dValue) 
                       { _x3dValue = x3dValue; }
private:
    FieldValue     *_value;
    FieldValue     *_x3dValue;
    ExposedField   *_exposedField;
    int             _eventIn;
    FieldValue     *_min;
    FieldValue     *_max;
    int             _nodeType;
    const char    **_strings;
};
#endif // _FIELD_H
