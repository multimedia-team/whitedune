/*
 * NodeArc2D.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeArc2D.h"
#include "Proto.h"
#include "FieldValue.h"
#include "Scene.h"
#include "SFFloat.h"
#include "RenderState.h"
#include "Util.h"
#include "math.h"
#include "DuneApp.h"

ProtoArc2D::ProtoArc2D(Scene *scene)
  : Proto(scene, "Arc2D")
{
    endAngle.set(
          addField(SFFLOAT, "endAngle", new SFFloat(M_PI / 2.0f), 
                   new SFFloat(-2.0f * M_PI), new SFFloat(2.0f * M_PI)));

    radius.set(
          addField(SFFLOAT, "radius", new SFFloat(1.0F), new SFFloat(0.0f)));

    startAngle.set(
          addField(SFFLOAT, "startAngle", new SFFloat(0.0F), 
                   new SFFloat(-2.0f * M_PI), new SFFloat(2.0f * M_PI)));
}

Node *
ProtoArc2D::create(Scene *scene)
{ 
    return new NodeArc2D(scene, this); 
}

NodeArc2D::NodeArc2D(Scene *scene, Proto *def)
  : ChainBasedNode(scene, def)
{
    _chainDirty = true;
}

void
NodeArc2D::createChain(void)
{
    int tess = TheApp->getTessellation() + 1;
    _chain.resize(tess);  
    float fradius = radius()->getValue();
    float fstartAngle = startAngle()->getValue();
    float fendAngle = endAngle()->getValue();
    if (fstartAngle == fendAngle) {
        fstartAngle = 0;
        fendAngle = 2 * M_PI;
    }
    float incAngle = (fendAngle - fstartAngle) / (tess - 1);
  

    for (int i = 0; i < tess; i++) {
        float angle = fstartAngle + i * incAngle;
        _chain[i].x = fradius * cos(angle);
        _chain[i].y = fradius * sin(angle);
        _chain[i].z = 0;
    }
}


