/*
 * NodeNurbsCurve2D.cpp
 *
 * Copyright (C) 2003 Th. Rothermel
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "resource.h"

#ifndef _WIN32
# include "stdlib.h"
#endif

#include "stdafx.h"
#include "NodeNurbsCurve2D.h"
#include "Scene.h"
#include "FieldValue.h"
#include "SFInt32.h"
#include "MFFloat.h"
#include "MFInt32.h"
#include "MFVec2f.h"
#include "MFVec3f.h"
#include "SFNode.h"
#include "SFBool.h"
#include "SFVec3f.h"
#include "Vec2f.h"
#include "Vec3f.h"
#include "RenderState.h"
#include "DuneApp.h"
#include "NurbsCurveDegreeElevate.h"
#include "Util.h"

ProtoNurbsCurve2D::ProtoNurbsCurve2D(Scene *scene)
  : Proto(scene, "NurbsCurve2D")
{

    controlPoint.set(
          addExposedField(MFVEC2F, "controlPoint", new MFVec2f()));
    tessellation.set(
          addExposedField(SFINT32, "tessellation", new SFInt32(0)));
    weight.set(
          addExposedField(MFFLOAT, "weight", new MFFloat(), new SFFloat(0.0f)));
    closed.set(
          addField(SFBOOL, "closed", new SFBool(false)));
    setFieldFlags(closed, FF_X3D_ONLY);
    knot.set(
          addField(MFFLOAT, "knot", new MFFloat()));
    order.set(
          addField(SFINT32, "order", new SFInt32(3), new SFInt32(2)));
}

Node *
ProtoNurbsCurve2D::create(Scene *scene)
{
    return new NodeNurbsCurve2D(scene, this); 
}

NodeNurbsCurve2D::NodeNurbsCurve2D(Scene *scene, Proto *proto)
  : Node(scene, proto)
{
    _isInternal = false;
    _chain.resize(0);
    _chainDirty = true;
}

void
NodeNurbsCurve2D::setField(int index, FieldValue *value) 
{
    _chainDirty = true;
    Node::setField(index, value);
}

int
NodeNurbsCurve2D::writeProto(int f)
{
    RET_ONERROR( mywritestr(f ,"EXTERNPROTO NurbsCurve2D[\n") )    
    TheApp->incSelectionLinenumber();
    RET_ONERROR( writeProtoArguments(f) )
    RET_ONERROR( mywritestr(f ," ]\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ,"[\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:web3d:vrml97:node:NurbsCurve2D\",\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:inet:blaxxun.com:node:NurbsCurve2D\",\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:ParaGraph:NurbsCurve2D\",\n") )
    TheApp->incSelectionLinenumber();
#ifdef HAVE_VRML97_AMENDMENT1_PROTO_URL
    RET_ONERROR( mywritestr(f ," \"") )
    RET_ONERROR( mywritestr(f ,HAVE_VRML97_AMENDMENT1_PROTO_URL) )
    RET_ONERROR( mywritestr(f ,"/NurbsCurve2DPROTO.wrl") )
    RET_ONERROR( mywritestr(f ,"\"\n") )
    TheApp->incSelectionLinenumber();
#else
    RET_ONERROR( mywritestr(f ," \"NurbsCurve2DPROTO.wrl\",\n") )
    TheApp->incSelectionLinenumber();
#endif
    RET_ONERROR( mywritestr(f ," \"http://129.69.35.12/dune/docs/vrml97Amendment1/NurbsCurve2DPROTO.wrl\"\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ,"]\n") )
    TheApp->incSelectionLinenumber();
    return 0;
}

int             
NodeNurbsCurve2D::write(int filedes, int indent)
{
    if (!_scene->isPureVRML())
        RET_ONERROR( NodeData::write(filedes, indent) )
    return 0;
}

void
NodeNurbsCurve2D::drawHandles()
{
    int iDimension = controlPoint()->getSize() / 2;
    RenderState state;

    if (weight()->getSize() != iDimension) {
        return;
    }

    glPushName(iDimension + 1);
    glDisable(GL_LIGHTING);
    Util::myGlColor3f(1.0f, 1.0f, 1.0f);
    glLoadName(NO_HANDLE);
    glBegin(GL_LINE_STRIP);
    for (int i = 0; i < iDimension; i++) {
        const float *v = controlPoint()->getValue(i);
        float w = weight()->getValue(i);
        glVertex3f(v[0] / w, drawXz() ? 0 : v[1] / w, drawXz() ? v[1] / w : 0);
    }
    glEnd();
    
    int ci;

    state.startDrawHandles();
    for (ci = 0; ci < iDimension; ci++) {
        state.setHandleColor(_scene, ci);
        glLoadName(ci);
        const float *v = controlPoint()->getValue(ci);
        float w = weight()->getValue(ci);
        state.drawHandle(Vec3f(v[0] / w, drawXz() ? 0 : v[1] / w, 
                                         drawXz() ? v[1] / w : 0));
    }
    state.endDrawHandles();
    glPopName();
    glEnable(GL_LIGHTING);
}


Vec3f
NodeNurbsCurve2D::getHandle(int handle, int *constraint, int *field)
{
    *field = controlPoint_Field() ;

    if (handle >= 0 && handle < controlPoint()->getSize() / 2) {
        const float *p = controlPoint()->getValue(handle);
        float w = weight()->getValue(handle);
        if (w != 0) {
            Vec3f ret(p[0] / w, drawXz() ? 0 : p[1] / w, 
                                drawXz() ? p[1] / w : 0);
            TheApp->PrintMessageWindowsVertex(IDS_VERTEX_SELECTED, 
                                              "controlPoint", handle);
            return ret;
        }
    } 
    *field = -1;
    return Vec3f(0.0f, 0.0f, 0.0f);
}

void
NodeNurbsCurve2D::setHandle(int handle, const Vec3f &v)
{
    MFVec2f *newValue = new MFVec2f(*controlPoint());

    int numPoints = controlPoint()->getSize() / 2; 
    if (handle >= 0 && handle < numPoints) {
        float w = weight()->getValue(handle);
        if (w != 0) {
            newValue->setValue(handle * 2    , v.x * w);
            newValue->setValue(handle * 2 + 1, (drawXz() ? v.z : v.y) * w);
            _scene->setField(this, controlPoint_Field(), newValue);
        }
    }
    if (_isInternal && hasParent())
        getParent()->update();
}

void
NodeNurbsCurve2D::update(void) 
{ 
    _chainDirty = true; 
    if (_isInternal && hasParent())
        getParent()->update();
}

void
NodeNurbsCurve2D::flip(int index)
{
    if (controlPoint())
        controlPoint()->flip(index);
    _chainDirty = true;
    
}

void
NodeNurbsCurve2D::swap(int fromTo)
{
    if (controlPoint())
        controlPoint()->swap(fromTo);
    _chainDirty = true;
    
}


