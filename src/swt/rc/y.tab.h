
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     BEG = 258,
     END = 259,
     ACCELERATORS = 260,
     VIRTKEY = 261,
     ASCII = 262,
     NOINVERT = 263,
     SHIFT = 264,
     CONTROL = 265,
     ALT = 266,
     BITMAP = 267,
     CURSOR = 268,
     DIALOG = 269,
     DIALOGEX = 270,
     EXSTYLE = 271,
     CAPTION = 272,
     CLASS = 273,
     STYLE = 274,
     AUTO3STATE = 275,
     AUTOCHECKBOX = 276,
     AUTORADIOBUTTON = 277,
     CHECKBOX = 278,
     COMBOBOX = 279,
     CTEXT = 280,
     DEFPUSHBUTTON = 281,
     EDITTEXT = 282,
     GROUPBOX = 283,
     LISTBOX = 284,
     LTEXT = 285,
     PUSHBOX = 286,
     PUSHBUTTON = 287,
     RADIOBUTTON = 288,
     RTEXT = 289,
     SCROLLBAR = 290,
     STATE3 = 291,
     USERBUTTON = 292,
     BEDIT = 293,
     HEDIT = 294,
     IEDIT = 295,
     FONT = 296,
     ICON = 297,
     LANGUAGE = 298,
     CHARACTERISTICS = 299,
     VERSIONK = 300,
     MENU = 301,
     MENUEX = 302,
     MENUITEM = 303,
     SEPARATOR = 304,
     POPUP = 305,
     CHECKED = 306,
     GRAYED = 307,
     HELP = 308,
     INACTIVE = 309,
     MENUBARBREAK = 310,
     MENUBREAK = 311,
     ES_READONLY = 312,
     ES_MULTILINE = 313,
     ES_PASSWORD = 314,
     TOOLBAR = 315,
     BUTTON = 316,
     MESSAGETABLE = 317,
     RCDATA = 318,
     STRINGTABLE = 319,
     VERSIONINFO = 320,
     FILEVERSION = 321,
     PRODUCTVERSION = 322,
     FILEFLAGSMASK = 323,
     FILEFLAGS = 324,
     FILEOS = 325,
     FILETYPE = 326,
     FILESUBTYPE = 327,
     BLOCKSTRINGFILEINFO = 328,
     BLOCKVARFILEINFO = 329,
     VALUE = 330,
     BLOCK = 331,
     MOVEABLE = 332,
     FIXED = 333,
     PURE = 334,
     IMPURE = 335,
     PRELOAD = 336,
     LOADONCALL = 337,
     DISCARDABLE = 338,
     NOT = 339,
     QUOTEDSTRING = 340,
     STRING = 341,
     NUMBER = 342,
     SIZEDSTRING = 343,
     DLGINIT = 344,
     NEGATE = 345
   };
#endif
/* Tokens.  */
#define BEG 258
#define END 259
#define ACCELERATORS 260
#define VIRTKEY 261
#define ASCII 262
#define NOINVERT 263
#define SHIFT 264
#define CONTROL 265
#define ALT 266
#define BITMAP 267
#define CURSOR 268
#define DIALOG 269
#define DIALOGEX 270
#define EXSTYLE 271
#define CAPTION 272
#define CLASS 273
#define STYLE 274
#define AUTO3STATE 275
#define AUTOCHECKBOX 276
#define AUTORADIOBUTTON 277
#define CHECKBOX 278
#define COMBOBOX 279
#define CTEXT 280
#define DEFPUSHBUTTON 281
#define EDITTEXT 282
#define GROUPBOX 283
#define LISTBOX 284
#define LTEXT 285
#define PUSHBOX 286
#define PUSHBUTTON 287
#define RADIOBUTTON 288
#define RTEXT 289
#define SCROLLBAR 290
#define STATE3 291
#define USERBUTTON 292
#define BEDIT 293
#define HEDIT 294
#define IEDIT 295
#define FONT 296
#define ICON 297
#define LANGUAGE 298
#define CHARACTERISTICS 299
#define VERSIONK 300
#define MENU 301
#define MENUEX 302
#define MENUITEM 303
#define SEPARATOR 304
#define POPUP 305
#define CHECKED 306
#define GRAYED 307
#define HELP 308
#define INACTIVE 309
#define MENUBARBREAK 310
#define MENUBREAK 311
#define ES_READONLY 312
#define ES_MULTILINE 313
#define ES_PASSWORD 314
#define TOOLBAR 315
#define BUTTON 316
#define MESSAGETABLE 317
#define RCDATA 318
#define STRINGTABLE 319
#define VERSIONINFO 320
#define FILEVERSION 321
#define PRODUCTVERSION 322
#define FILEFLAGSMASK 323
#define FILEFLAGS 324
#define FILEOS 325
#define FILETYPE 326
#define FILESUBTYPE 327
#define BLOCKSTRINGFILEINFO 328
#define BLOCKVARFILEINFO 329
#define VALUE 330
#define BLOCK 331
#define MOVEABLE 332
#define FIXED 333
#define PURE 334
#define IMPURE 335
#define PRELOAD 336
#define LOADONCALL 337
#define DISCARDABLE 338
#define NOT 339
#define QUOTEDSTRING 340
#define STRING 341
#define NUMBER 342
#define SIZEDSTRING 343
#define DLGINIT 344
#define NEGATE 345




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 57 "rcparse.y"

    RCNode *node;
    int i;
    const char *s;



/* Line 1676 of yacc.c  */
#line 240 "y.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


