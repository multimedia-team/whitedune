/*
 * NodeClipPlane.cpp
 *
 * Copyright (C) 2009 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeClipPlane.h"
#include "Proto.h"
#include "FieldValue.h"
#include "SFBool.h"
#include "SFVec4f.h"
#include "DuneApp.h"

ProtoClipPlane::ProtoClipPlane(Scene *scene)
  : Proto(scene, "ClipPlane")
{
    enabled.set(
        addExposedField(SFBOOL, "enabled", new SFBool(true)));
    plane.set(
        addExposedField(SFVEC4F, "plane", new SFVec4f(0, 1, 0, 0)));
}

Node *
ProtoClipPlane::create(Scene *scene)
{ 
    return new NodeClipPlane(scene, this); 
}

NodeClipPlane::NodeClipPlane(Scene *scene, Proto *def)
  : Node(scene, def)
{
}
