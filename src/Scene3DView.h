/*
 * Scene3DView.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _SCENE3DVIEW_H
#define _SCENE3DVIEW_H

#include "Vec3f.h"
#include "Quaternion.h"
#include "Matrix.h"
#include "SceneView.h"
#include "InputDevice.h"

class SFVec3f;

#include "swttypedef.h"

class Scene;

class Scene3DView : public SceneView
{
public:
                        Scene3DView(Scene *scene, SWND parent);
    virtual            ~Scene3DView();
    virtual void        OnDraw(int x, int y, int width, int height);
    virtual void        OnKeyDown(int key, int x, int y, int modifiers);
    virtual void        OnMouseEnter();
    virtual void        OnMouseLeave();
    virtual void        OnMouseMove(int x, int y, int modifiers);
    virtual void        OnLButtonDown(int x, int y, int modifiers);
    virtual void        OnMButtonDown(int x, int y, int modifiers);
    virtual void        OnRButtonDown(int x, int y, int modifiers);
    virtual void        OnLButtonUp(int x, int y, int modifiers);
    virtual void        OnMButtonUp(int x, int y, int modifiers);
    virtual void        OnUpdate(SceneView *sender, int type, Hint *hint);
    virtual void        OnSize(int width, int height);

    const char         *getName(void) const { return "Scene3DView"; }

    void                startWalking(bool walkForward);
    void                startWalking(void);
    void                stopWalking(void);
    int                 walk();

    int                 readInputDevice(void);
    void                Transform3D(const Path* path, InputDevice* inputDevice);
    void                Navigate3D(InputDevice * inputDevice);
    void                Handle3D(const Path* path,InputDevice* inputDevice, 
                                 int handle);

protected:
    Quaternion         &getQuat(const Path* path);
    float               constrainLine(float x1, float y1, float z1,
                                      float x2, float y2, float z2) const;
    void                constrainPlane(float x1, float y1, float z1,
                                       float x2, float y2, float z2,
                                       float *x, float *y) const;
    Vec3f               constrainSphere(const Vec3f &euler, float dx, float dy,
                                        const Matrix &mat);
    bool                destroySelf() { return false; }


    // Generated message map functions
protected:
    SDC         _dc;
    SGLC        _glc;
    bool        _trackMouse;
    int         _trackX;
    int         _trackY;

    Vec3f       _offset;
    bool        _backedUp;
    SFVec3f    *_oldCameraPos;
    Vec3f       _trackBase;
    SCURSOR     _cursorArrow;
    SCURSOR     _cursorZoom;
    SCURSOR     _cursorRotate;
    SCURSOR     _cursorRoll;
    SCURSOR     _cursorMove;
    SCURSOR     _cursorNone;
    Matrix      _handleMatrix;

    bool        _button1down;
    bool        _button2down;

    STIMER      _inputDeviceTimer;

    STIMER      _walkTimer;
    Vec3f       _walk;
    bool        _walking;
    bool        _walkForward;

    int         _width;
    int         _height;

    bool        _fieldEdit;

    int         _mouseX;
    int         _mouseY;

    bool        _hitHandle;
};

#endif // !defined(_SCENE3DVIEW_H)
