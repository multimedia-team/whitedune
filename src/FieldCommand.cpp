/*
 * FieldCommand.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "FieldCommand.h"
#include "Scene.h"

#include "Node.h"
#include "FieldValue.h"

FieldCommand::FieldCommand(Node *node, int field, FieldValue *newValue)
{
    _node = node;
    _field = field;
    if (newValue) {
        _newValue = newValue;
        _newValue->ref();
        node->getScene()->setField(node, field, newValue);
    } else {
        _newValue = node->getField(field);
        _newValue->ref();
    }
    _oldValue = node->getField(field);
    _oldValue->ref();
}

FieldCommand::~FieldCommand()
{
    _newValue->unref();
    _oldValue->unref();
}

void
FieldCommand::execute()
{
    _oldValue->unref();
    _oldValue = _node->getField(_field);
    _oldValue->ref();
    _node->setField(_field, _newValue);
    _node->getScene()->OnFieldChange(_node, _field);
}

#include "NodeNurbsSurface.h"
#include "MFVec3f.h"

void
FieldCommand::undo()
{
    _newValue->unref();
    _newValue = _node->getField(_field);
    _newValue->ref();
    _node->setField(_field, _oldValue);
    _node->getScene()->OnFieldChange(_node, _field);
}
