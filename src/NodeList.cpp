/*
 * NodeList.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifndef _WIN32
# include <fcntl.h>
#endif

#include "swt.h"
#include "NodeList.h"
#include "Node.h"
#include "Scene.h"

static bool clearFlags(Node *node, void *data)
{
    int flag = *((int *)data);
    node->clearFlag(flag);
    return true;
}

void
NodeList::clearFlag(int flag) const
{
    for (int i = 0; i < size(); i++)
        _data[i]->doWithBranch(clearFlags, &flag);
}

static bool setFlags(Node *node, void *data)
{
    int flag = *((int *)data);
    node->setFlag(flag);
    return true;
}

void
NodeList::setFlag(int flag) const
{
    for (int i = 0; i < size(); i++)
        _data[i]->doWithBranch(setFlags, &flag);
}

bool
NodeList::canWriteAc3d()
{
    for (int i = 0; i < size(); i++) {
        Node *node = get(i);
        if (node != NULL)
            if (node->canWriteAc3d())
                return true;
    }
    return false;
}

int 
NodeList::writeAc3d(int f, int indent) const
{
    int kids = 0;
    int i;
    for (i = 0; i < size(); i++)
        if (get(i)->canWriteAc3d())
            kids++;

    if (kids == 0)
        return 0;

    RET_ONERROR( mywritef(f, "OBJECT group\n") )

    RET_ONERROR( mywritef(f, "kids %d\n", kids) )        

    for (i = 0; i < size(); i++)
        get(i)->writeAc3d(f, indent);

    return 0; 
}

bool    
NodeList::canWriteCattGeo()
{
    for (int i = 0; i < size(); i++) {
        Node *node = get(i);
        if (node != NULL)
            if (node->canWriteCattGeo())
                return true;
    }
    return false;
}

// see dangerous contruct in NodeSwitch::writeCattGeo, 
// if node should deliver other things like name, scene etc. 

int
NodeList::writeCattGeo(Node *node, int filedes, int indent)
{ 
    int children = 0;
    int i;
    for (i = 0; i < size(); i++)
        if (get(i)->canWriteCattGeo())
            children++;

    if (children == 0)
        return 0;

    Scene *scene = node->getScene();
    char *path = scene->getCattGeoPath();
    int len = strlen(path)+256;
    char *filename = new char[len];
    if (node == scene->getRoot())
        mysnprintf(filename, len - 1, "%sMaster.geo", path);
    else
        mysnprintf(filename, len - 1, "%s%s%d.geo", path, 
                   (const char *) node->getProto()->getName(scene->isX3d()),
                   scene->getAndIncCattGeoFileCounter());
    int f = open(filename, O_WRONLY | O_CREAT, 00666);
    if (f == -1)
        return -1;

    RET_ONERROR( mywritef(f, "OBJECT") )
    RET_ONERROR( mywritef(f, swGetLinefeedString()) )

    if (node->hasName())
        RET_ONERROR( mywritef(f, "; grouping node with vrml97 DEF name: %s", 
                              node->getName().getData()) )
    RET_ONERROR( mywritef(f, swGetLinefeedString()) )

    if (node == scene->getRoot()) {
        RET_ONERROR( mywritef(f, "INCLUDE ") )
        RET_ONERROR( mywritef(f, "material.geo") )
        RET_ONERROR( mywritef(f, swGetLinefeedString()) )
    }

    for (i = 0; i < size(); i++) {
        if (get(i)->canWriteCattGeo()) {
            if ((get(i)->getType() == DUNE_CATT_EXPORT_REC) ||
                (get(i)->getType() == DUNE_CATT_EXPORT_SRC)) {
                RET_ONERROR(get(i)->writeCattGeo(filedes, indent) )
                continue;
            }

            const char *protoName = get(i)->getProto()->getName(false);
            mysnprintf(filename, len - 1, "%s%d.geo", protoName, 
                       scene->getCattGeoFileCounter());
            RET_ONERROR( mywritef(f, "INCLUDE ") )
            RET_ONERROR( mywritef(f, filename) )
            RET_ONERROR( mywritef(f, swGetLinefeedString()) )            
            RET_ONERROR(get(i)->writeCattGeo(filedes, indent) )
        }
    }
    RET_ONERROR( mywritef(f, "CORNERS") )
    RET_ONERROR( mywritef(f, swGetLinefeedString()) )    
    RET_ONERROR( mywritef(f, "PLANES") )
    RET_ONERROR( mywritef(f, swGetLinefeedString()) )    

    delete [] filename;

    return swTruncateClose(f);
}

bool
NodeList::canWriteLdrawDat()
{
    for (int i = 0; i < size(); i++) {
        Node *node = get(i);
        if (node != NULL)
            if (node->canWriteLdrawDat())
                return true;
    }
    return false;
}

int 
NodeList::writeLdrawDat(int f, int indent) const
{
    for (int i = 0; i < size(); i++)
        RET_ONERROR( get(i)->writeLdrawDat(f, indent) )
    return 0; 
}

