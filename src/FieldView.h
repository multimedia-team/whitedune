/*
 * FieldView.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _FIELD_VIEW_H
#define _FIELD_VIEW_H

#ifndef _ARRAY_H
#include "Array.h"
#endif

#ifndef _SCENE_VIEW_H
#include "SceneView.h"
#endif

#ifndef _RECT_H
#include "Rect.h"
#endif

#ifndef _POINT_H
#include "Point.h"
#endif

#include "SFMFTypes.h"

class FieldViewItem;
class DuneDoc;
class Node;
class FieldValue;
class Hint;

#include "swttypedef.h"

class FieldView : public SceneView
{
public:
                        FieldView(Scene *scene, SWND wnd);
        virtual        ~FieldView();

public:

        virtual void    OnUpdate(SceneView *sender, int type, Hint *hint);
        virtual void    OnDraw(int x, int y, int width, int height);
        virtual void    OnSize(int width, int height);
        virtual void    OnLButtonDown(int x, int y, int modifiers);
        virtual void    OnLButtonUp(int x, int y, int modifiers);
        virtual void    OnMouseMove(int x, int y, int modifiers);

        const char     *getName(void) const { return "FieldView"; }

        void            DeleteView();
        void            DeleteFields();

        void            OnHeaderChange(int pos, int width);

        int             GetColumnWidth(int column) const;
        SFONT           GetUFont() const { return _uFont; }
        int             GetFloatWidth() const { return _floatWidth; }

        int             GetItemWidth() const { return GetColumnWidth(1); }
        int             GetItemHeight() const { return _itemHeight; }

// virtual BOOL PreTranslateMessage(MSG* pMsg);
// virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);

        void            DrawItem(int item, SDC dc);
        void            StartEditing();
        void            StopEditing();
        void            AbortEditing();
        void            StartTracking();
        void            StopTracking();
        void            SetColumnWidth(int column, int width);
        void            GetItemRect(int i, Rect *r);
        void            GetFieldRect(int field, int index, Rect *r);
        int             HitTest(int x, int y);
        void            MoveControls(int left);
        void            UpdateAll();
        void            SelectField(int field) {_selectedField=field;}
        int             GetSelectedField(void) {return _selectedField;}
        Node           *GetSelectedNode(void) {return _selectedNode;}
        int             FindField(int field);

        void            RefreshItemList();
        void            ChangeValue(FieldViewItem *item, FieldValue *newValue);
        void            UpdateBars();
        void            DeleteLastSelection(void);

        virtual bool    isFieldView(void) { return true; }

//  void OnItemclick(NMHDR* pNMHDR, LRESULT* pResult);
//  void OnLButtonDblClk(UINT nFlags, CPoint point);
//  void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
//  void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
//  void OnButtonClicked(UINT id);
//  void OnButtonKillfocus(UINT id);

        void            UpdateTrackingCursor(int x, int y);

protected:

        enum            FVState { 
                            NORMAL, 
                            WAIT_EDIT, 
                            WAIT_TRACK, 
                            TRACKING, 
                            EDITING 
                        };

        SWND            _window;
        SWND            _scroller;
        int             _lastXPosition;
        int             _lastYPosition;
        int             _maxYPosition;

        int             _autoScrolling;
        int             _autoScrollPY;
        int             _autoScrollDY;

        Node           *_selectedNode;
        int             _selectedField;
        int             _selectedIndex; // for MField's
        int             _selectedOffset;
        int             _selectedItem;
        SWND            _edit;
        SHEADER         _header;
        SBITMAP         _halftoneBitmap;
        int             _itemHeight;
        int             _height;
        int             _width;
        int             _floatWidth;
        int             _pageHeight;
        SFONT           _uFont;
        int             _scrollY; // in lines
        float           _scrollRatio;

        FVState         _state;

        Point           _trackPoint;
        FieldValue     *_trackValue;

        Array<FieldViewItem *> _items;
        Array<FieldViewItem *> _fields;

        bool            _fieldViewActive;
        bool            _isRoot;

        SCURSOR         _cursorArrow;
        SCURSOR         _cursorHMove;
 
        bool            _cursorIsArrow;
};

#endif // !_FIELD_VIEW_H
