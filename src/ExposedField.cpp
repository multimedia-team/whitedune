/*
 * ExposedField.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "ExposedField.h"
#include "Scene.h"
#include "FieldValue.h"
#include "Field.h"   // for FF_HIDDEN

ExposedField::ExposedField(int type, const MyString &name, FieldValue *value, 
                           FieldValue *min, FieldValue *max,
                           int nodeType, int flags, const char **strings,
                           const MyString &x3dName)
{
    _type = type;
    _name = name;
    _x3dName = x3dName;
    _value = value;
    if (_value) _value->ref();
    _min = min;
    if (_min) _min->ref();
    _max = max;
    if (_max) _max->ref();
    _nodeType = nodeType;
    _flags = flags;
    _strings = strings;
}

ExposedField::~ExposedField()
{
    if (_value) _value->unref();
    if (_min) _min->unref();
    if (_max) _max->unref();
}

const char *
ExposedField::getElementName(bool x3d) const
{
    if (x3d)
        return "inputOutput";
    else
        return "exposedField";
}

int 
ExposedField::write(int f, int indent, int flags) const
{
    if (_flags & (FF_HIDDEN | FF_STATIC)) return(0);

    if (isX3dXml(flags))
        RET_ONERROR( indentf(f, indent) )
    RET_ONERROR( writeElementPart(f, indent, flags) )
    if (isX3dXml(flags)) {
        bool nodeField = (_type == SFNODE) || (_type == MFNODE);
        if (flags & WITHOUT_VALUE)
            RET_ONERROR( mywritestr(f, " />\n") )
        else if (nodeField) {
            RET_ONERROR( mywritestr(f, ">\n") )
            TheApp->incSelectionLinenumber();
            if (_value) 
                RET_ONERROR( _value->writeXml(f, indent) )
            RET_ONERROR( indentf(f, indent) )
            RET_ONERROR( indentf(f, indent) )
            RET_ONERROR( mywritestr(f, "</field>\n") )
            TheApp->incSelectionLinenumber();
        } else {
            if (_value) {
                RET_ONERROR( mywritestr(f, " value='") )
                RET_ONERROR( _value->write(f, 0) )
                RET_ONERROR( mywritestr(f, "'") )
            }
            RET_ONERROR( mywritestr(f, " />\n") )
        }
    } else {
        if (flags & WITHOUT_VALUE)
            RET_ONERROR( mywritestr(f ,"\n") )
        else {
            RET_ONERROR( mywritestr(f ," ") )
            if (_value) 
                RET_ONERROR( _value->write(f, indent) )
        }
    }
    TheApp->incSelectionLinenumber();
    return 0;
}


