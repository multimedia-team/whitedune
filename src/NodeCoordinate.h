/*
 * NodeCoordinate.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_COORDINATE_H
#define _NODE_COORDINATE_H

#ifndef _NODE_H
#include "Node.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif

#include "SFMFTypes.h"

class NodeNurbsGroup;

class ProtoCoordinate : public Proto {
public:
                    ProtoCoordinate(Scene *scene);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return VRML_COORDINATE; }
    virtual int     getNodeClass() const { return COORDINATE_NODE; }

    FieldIndex point;
};

class NodeCoordinate : public Node {
public:
                    NodeCoordinate(Scene *scene, Proto *proto);

    virtual int     getProfile(void) const { return PROFILE_INTERCHANGE; }
    virtual Node   *copy() const { return new NodeCoordinate(*this); }

    void            drawHandles(void);
    Vec3f           getHandle(int handle, int *constraint, int *field);
    void            setHandle(int handle, const Vec3f &v);
    virtual bool    isInvalidChildNode(void) { return true; }
    virtual void    setField(int index, FieldValue *value);
    virtual bool    validHandle(int handle);

    void            setHandle(MFVec3f *value, int handle,
                              const Vec3f &newV, const Vec3f &oldV,
                              bool already_changed);

    virtual void    update();

    virtual void    flip(int index);
    virtual void    swap(int fromTo);
    virtual void    flatten(int direction, bool zero);              
    virtual bool    canFlatten(void) {return true;}

    virtual bool    hasTwoSides(void);
    virtual bool    isDoubleSided(void);
    virtual void    toggleDoubleSided(void);
    virtual void    flipSide(void);
    
    fieldMacros(MFVec3f,  point, ProtoCoordinate)

    void draw(Node *node);
    NodeNurbsGroup *findNurbsGroup();
};

#endif // _NODE_COORDINATE_H

