/*
 * GeometryNode.h
 *
 * Copyright (C) 1999 Stephen F. White, 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _GEOMETRY_NODE_H
#define _GEOMETRY_NODE_H

#ifndef _NODE_H
#include "Node.h"
#endif
#include "NodeAppearance.h"
#include "NodeMaterial.h"
#include "NodeImageTexture.h"
#include "NodeTextureTransform.h"

class GeometryProto : public Proto {
public:
                    GeometryProto(Scene *scene, const char *name) : 
                          Proto(scene, name) {}

    virtual int     getNodeClass() const { return GEOMETRY_NODE; }
};

class GeometryNode : public Node {
public:
                         GeometryNode(Scene *scene, Proto *proto);
public:
    NodeAppearance       *getAppearance(void);
    NodeMaterial         *getMaterial(void);
    NodeImageTexture     *getImageTexture(void);
    NodeTextureTransform *getTextureTransform(void); 
};

#endif // _GEOMETRY_NODE_H

