/*
 * NodeOdeMotorJoint.h
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_ODE_MOTOR_JOINT_H
#define _NODE_ODE_MOTOR_JOINT_H

#include "NodeMotorJoint.h"

#include "SFMFTypes.h"

class ProtoOdeMotorJoint : public ProtoMotorJoint {
public:
                    ProtoOdeMotorJoint(Scene *scene);

    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return DUNE_ODE_MOTOR_JOINT; }

    FieldIndex fMax1;
    FieldIndex fMax2;
    FieldIndex fMax3;
};

class NodeOdeMotorJoint : public NodeMotorJoint {
public:
                    NodeOdeMotorJoint(Scene *scene, Proto *proto);

    virtual Node   *copy() const { return new NodeOdeMotorJoint(*this); }

    fieldMacros(SFFloat, fMax1,                ProtoOdeMotorJoint)
    fieldMacros(SFFloat, fMax2,                ProtoOdeMotorJoint)
    fieldMacros(SFFloat, fMax3,                ProtoOdeMotorJoint)
};

#endif 
