/*
 * StartWithApp.cpp
 *
 * Copyright (C) 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <errno.h>
#include "stdafx.h"
#include <math.h>

#include "DuneApp.h"
#include "StartWithApp.h"
#include "PreferencesApp.h"

StartWithApp::StartWithApp()
   {
   _startVariant = NULL;
   _startLanguage = NULL;
   }

void
StartWithApp::StartWithSetDefaults()
   {
   if (_startVariant != NULL)
       free(_startVariant);
   _startVariant = strdup("dune");
   if (_startLanguage != NULL)
       free(_startLanguage);
   _startLanguage = strdup("en");
   }

void
StartWithApp::StartWithLoadPreferences()
   {
   assert(TheApp != NULL);

   StartWithSetDefaults();

   if (_startVariant != NULL)
       free(_startVariant);
   _startVariant = strdup(TheApp->GetPreference("StartWith", "dune"));

   if (_startLanguage != NULL)
       free(_startLanguage);
   _startLanguage = strdup(TheApp->GetPreference("StartLanguage", "en"));
   }

void
StartWithApp::StartWithSavePreferences()
   {
   assert(TheApp != NULL);

   if (_startVariant != NULL)
       TheApp->SetPreference("StartWith", _startVariant);
   if (_startLanguage != NULL)
       TheApp->SetPreference("StartLanguage", _startLanguage);
   }

