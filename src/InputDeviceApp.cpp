/*
 * InputDeviceApp.cpp
 *
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <errno.h>
#include "stdafx.h"

#include "InputDeviceApp.h"

// sort inputdevices
//
// make inputdevices without readdelay comes first in array
//
// so inputdevices with a readdelay can prepare the read, 
// while other inputdevices can use the time to read data

void
InputDeviceApp::sortInputDevices(void)
{
    // only a few devices, it is ok to use bubblesort 
    bool changedflag=false;
    do {
       InputDevice* tmp;
       changedflag=false;
       for (int i=0;i<_inputDevices.size()-1;i++)
          if (!_inputDevices[i]->hasReadDelay() && 
               _inputDevices[i+1]->hasReadDelay()) {
             tmp=_inputDevices[i];
             _inputDevices[i]=_inputDevices[i+1];
             _inputDevices[i+1]=tmp;
             changedflag=true;
          }
    } while (changedflag==true);
}


