/*
 * Box2NurbsDialog.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2003 Thomas Rothermel
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"
#include "Box2NurbsDialog.h"
#include <stdio.h>
#include <stdlib.h>
#include "swt.h"
#include "resource.h"
#include "DuneApp.h"
#include "NodeBox.h"

Box2NurbsDialog::Box2NurbsDialog(SWND parent, 
                                 int nuAreaPoints,
                                 int nvAreaPoints,
                                 int nzAreaPoints,
                                 int uDegree, int vDegree)
  : Dialog(parent, IDD_BOX2NURBS)
{
    _nuAreaPoints = nuAreaPoints;
    _nvAreaPoints = nvAreaPoints;
    _nzAreaPoints = nzAreaPoints;
    _uDegree = uDegree;
    _vDegree = vDegree;
    _have6Planes = false;
    LoadData();
}

Box2NurbsDialog::~Box2NurbsDialog()
{
}

void
Box2NurbsDialog::SaveData()
{
    char buf[128];

    if (swGetCheck(swGetDialogItem(_dlg, IDC_6PLANES)))
        _have6Planes = true;
    else
        _have6Planes = false;
    swGetText(swGetDialogItem(_dlg, IDC_BOX_NZAREAPOINTS), buf, 128);
    _nzAreaPoints = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_BOX_NUAREAPOINTS), buf, 128);
    _nuAreaPoints = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_BOX_NVAREAPOINTS), buf, 128);
    _nvAreaPoints = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_BOX_U_DEGREE), buf, 128);
    _uDegree = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_BOX_V_DEGREE), buf, 128);
    _vDegree = atoi(buf);
}

bool
Box2NurbsDialog::Validate()
{

   bool valid = true;

   if (_have6Planes) {
      if ((_uDegree < 1) || (_vDegree < 1))
          valid = false;
      if (_nuAreaPoints < 2)
          valid = false;
      if (_uDegree > (_nuAreaPoints - 1))
          valid = false;
      if (_nvAreaPoints < 2)
          valid = false;
      if (_vDegree > (_nvAreaPoints - 1))
          valid = false;
   } else {
       if ((_uDegree < 1) || (_vDegree < 1))
          valid = false;
       if (_nuAreaPoints < 2)
          valid = false;
       if (_uDegree > (_nuAreaPoints - 1))
          valid = false;
       if (_nvAreaPoints < 2)
          valid = false;
       if (_vDegree > (_nvAreaPoints - 1))
          valid = false;
       if (_nzAreaPoints < 2)     
          valid = false;
    }
    if (valid)
        valid =  _uDegree > 0 && _vDegree > 0;
    return valid;
}

void
Box2NurbsDialog::LoadData()
{
    char buf[128];

    swSetCheck(swGetDialogItem(_dlg, IDC_6PLANES), _have6Planes ? 1 : 0);
    mysnprintf(buf, 128, "%d", _nuAreaPoints);
    swSetText(swGetDialogItem(_dlg, IDC_BOX_NUAREAPOINTS), buf);
    mysnprintf(buf, 128, "%d", _nvAreaPoints);
    swSetText(swGetDialogItem(_dlg, IDC_BOX_NVAREAPOINTS), buf);
    mysnprintf(buf, 128, "%d", _nzAreaPoints);
    swSetText(swGetDialogItem(_dlg, IDC_BOX_NZAREAPOINTS), buf);
    mysnprintf(buf, 128, "%d", _uDegree);
    swSetText(swGetDialogItem(_dlg, IDC_BOX_U_DEGREE), buf);
    mysnprintf(buf, 128, "%d", _vDegree);
    swSetText(swGetDialogItem(_dlg, IDC_BOX_V_DEGREE), buf);
}
