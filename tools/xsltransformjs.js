//original author declares xlstransformjs.js public domain

var oArgs = WScript.Arguments;

if (oArgs.length == 0)
{
    WScript.Echo ("Usage : cscript xsltransformjs.js x3d xsl out");
    WScript.Quit();
}
xmlFile = oArgs(0); //+ ".xml";
xslFile = oArgs(1); //+ ".xsl";
wrlFile = oArgs(2);

var xsl = new ActiveXObject("MSXML2.DOMDOCUMENT.6.0");
var xml = new ActiveXObject("MSXML2.DOMDocument.6.0");
xml.validateOnParse = false;
xml.async = false;
// next few lines from dug, in anticipation of problems with DTD, scripts etc
// http://msdn.microsoft.com/en-us/library/ms763800(VS.85).aspx
xml.setProperty("AllowDocumentFunction", true);
xml.setProperty("AllowXsltScript", true);
xml.resolveExternals = true;
xml.setProperty("ProhibitDTD", false);  //if you have a <DTD> line in your X3D file
//end dug lines
xml.load(xmlFile);

if (xml.parseError.errorCode != 0)
    WScript.Echo ("XML Parse Error : " + xml.parseError.reason);

//how to do: x3d Collision enabled='true' --> vrml97 Collision collide TRUE

xsl.async = false;
// next few lines from dug, in anticipation of problems with DTD, scripts etc
// http://msdn.microsoft.com/en-us/library/ms763800(VS.85).aspx
xsl.setProperty("AllowDocumentFunction", true);
xsl.setProperty("AllowXsltScript", true);
xsl.resolveExternals = true;
xsl.setProperty("ProhibitDTD", false);  //for JScript
//end dug lines
xsl.load(xslFile);

if (xsl.parseError.errorCode != 0)
    WScript.Echo ("XSL Parse Error : " + xsl.parseError.reason);

try
{
    //WScript.Echo (xml.transformNode(xsl.documentElement));
	var str = xml.transformNode(xsl.documentElement);
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var a = fso.CreateTextFile(wrlFile, true);
	a.Write(str);
	a.Close();
	//WScript.StdOut.Write(str);
}
catch(err)
{
    WScript.Echo ("Transformation Error : " + err.number + "*" + err.description);
}