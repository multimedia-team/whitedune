Source: whitedune
Section: graphics
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Philippe Coval <rzr@users.sf.net>
Build-Depends:
 autoconf,
 autotools-dev,
 bison,
 debhelper-compat (= 9),
 flex,
 gawk,
 libgl1-mesa-dev | libgl-dev,
 libglu1-mesa-dev,
 libjpeg-dev,
 libmotif-dev,
 libpng-dev,
 libxi-dev,
 libxmu-dev,
 x11proto-core-dev
Standards-Version: 3.9.6
Homepage: http://vrml.cip.ica.uni-stuttgart.de/dune/
Vcs-Git: https://salsa.debian.org/multimedia-team/whitedune.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/whitedune

Package: whitedune
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Recommends: xfonts-100dpi | xfonts-100dpi-transcoded | xfonts-75dpi | xfonts-75dpi-transcoded
Suggests:
 whitedune-docs,
 x-www-browser
Description: graphical VRML97/X3D viewer, editor, 3D modeller and animation tool
 Whitedune can read VRML97 files, display and let the user change the
 scenegraph and all fields of all nodes.
 .
 The most powerful 3D modelling features of whitedune support the
 VRML97 Amendment1 style NURBS nodes and Superformula based PROTOs.
 This is mainly convenient for building some rounded shapes.
 .
 Whitedune supports some 3D inputdevices like joysticks, gamepads
 or all devices supported via the Xinput protocol and also quadbuffer stereo
 visuals.
 .

Package: whitedune-docs
Architecture: all
Depends: ${misc:Depends}
Section: doc
Multi-Arch: foreign
Description: documentation for whitedune
 This package contains useful documentation for the whitedune VRML editor
 .
 VRML97 (Virtual Reality Modelling Language) is the ISO standard for
 displaying 3D data over the web.
 .
 VRML has support for animation, realtime
 interaction and multimedia (image, movie, sound). VRML97 can be written
 by popular programs like maya, catia, 3D Studio MAX, cinema4D, blender,
 wings3d and others, but (unlike whitedune) most of this programs support
 only a small part of the VRML97 features.
 .
